#include "VSInstancedRendering.h"


VSInstancedRendering::VSInstancedRendering(std::shared_ptr<ServiceManager> p_serviceManager) : AbstractRenderer(p_serviceManager)
{
}

VSInstancedRendering::~VSInstancedRendering()
{
	if (m_vertexBufferObjectVector.size() != 0) cleanUpScene();
}

int VSInstancedRendering::initScene(const std::shared_ptr<AbstractScene>& p_currentScene)
{
	if (m_vertexBufferObjectVector.size() != 0) cleanUpScene();
	sceneSetup(2, p_currentScene);
	return 0;
}

int VSInstancedRendering::cleanUpScene()
{
	sceneCleanUp();
	return 0;
}


int VSInstancedRendering::init()
{
	if (initShaders("shaders/VSInstancedRendering.vert", "", "shaders/unlit_Texture.frag",
		"shaders/VSInstancedRendering.vert", "", "shaders/lit_LambertianDiffuse.frag",
		"shaders/VSInstancedRendering.vert", "", "shaders/lit_PBR.frag") == -1)
	{
		return -1;
	}

	initViewportArray();
	initViewandProjectionMatrices();

	return 0;
}

void VSInstancedRendering::refresh()
{
	glClearColor(0.2f, 0.7f, 0.2f, 1.0f);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDisable(GL_BLEND);
	glDisable(GL_SCISSOR_TEST);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_FALSE);
	glDepthMask(GL_TRUE);

	setCurrentShader("HighComplexity");
	glViewportArrayv(0, 2, m_viewPortArray.data());
}

void VSInstancedRendering::render()
{
	renderViewportArray();
}