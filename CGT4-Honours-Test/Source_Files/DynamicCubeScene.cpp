#include "DynamicCubeScene.h"

DynamicCubeScene::DynamicCubeScene(glm::uvec3 p_distribution) : m_distribution(p_distribution)
{
}

DynamicCubeScene::~DynamicCubeScene()
{
	cleanUP();
}

int DynamicCubeScene::init()
{
	if (importFiletoAiScene("models/1x1x1Cube.obj") != 0)
	{
		std::cout << "DynamicCubeScene::init() Error!: importFiletoAiScene failed to load models/1x1x1Cube.obj" << std::endl;
		return -1;
	}

	m_textureFilenameVector.push_back("textures/YellowTexture.BMP");
	m_textureCount++;
	m_textureFilenameVector.push_back("textures/CyanTexture.BMP");

	m_textureCount++;
	const GLuint renderableCount = m_distribution.x * m_distribution.y * m_distribution.z;
	m_perMeshInstanceCount.push_back(renderableCount);

	m_positionVector.reserve(renderableCount);
	m_modelMatrixVector.reserve(renderableCount);
	m_orientationVector.reserve(renderableCount);
	m_orientationVector.reserve(renderableCount);

	std::vector<float> xPositions, yPositions, zPositions;
	xPositions.reserve(m_distribution.x);
	yPositions.reserve(m_distribution.y);
	zPositions.reserve(m_distribution.z);

	for (size_t xCount = 0; xCount < m_distribution.x; ++xCount)
	{
		xPositions.push_back(static_cast<float>(xCount)* 2.0f - static_cast<float>(m_distribution.x) + 1.0f);
	}
	for (size_t yCount = 0; yCount < m_distribution.y; ++yCount)
	{
		yPositions.push_back(static_cast<float>(yCount)* 2.0f - static_cast<float>(m_distribution.y) + 1.0f);
	}
	for (size_t zCount = 0; zCount < m_distribution.x; ++zCount)
	{
		zPositions.push_back(static_cast<float>(zCount)* 2.0f - static_cast<float>(m_distribution.z) + 1.0f);
	}

	glm::mat4 modelMatrix;
	glm::vec4 position;
	for (size_t xPosition = 0; xPosition < m_distribution.x; ++xPosition)
	{
		for (size_t yPosition = 0; yPosition < m_distribution.y; ++yPosition)
		{
			for (size_t zPosition = 0; zPosition < m_distribution.z; ++zPosition)
			{

				position = glm::vec4(static_cast<float>(xPositions[xPosition]), static_cast<float>(yPositions[yPosition]), static_cast<float>(zPositions[zPosition]), 1.0f);
				m_positionVector.push_back(position);
				m_modelMatrixVector.push_back((glm::lookAt(glm::vec3(m_positionVector[m_renderableObjectCount]), glm::vec3(m_positionVector[m_renderableObjectCount]) + glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f))));
				m_orientationVector.push_back(glm::fquat());
				m_renderableObjectTextureIDVector.push_back(m_renderableObjectCount % 2);
				m_renderableObjectCount++;
			}
		}
	}

	return 0;
}

int DynamicCubeScene::cleanUP()
{
	for (const aiScene* aiScene : m_aiSceneVector)
	{
		aiReleaseImport(aiScene);
	}
	return 0;
}
