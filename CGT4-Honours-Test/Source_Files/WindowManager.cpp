#include "WindowManager.h"

WindowManager::WindowManager(std::shared_ptr<ServiceManager> p_serviceManager) : m_serviceManager(p_serviceManager), m_glcontext(nullptr)
{
}

WindowManager::~WindowManager()
{
	SDL_GL_DeleteContext(m_glcontext);
}

int WindowManager::init()
{
	std::cout << "SDL Reported Platform: " << SDL_GetPlatform() << std::endl;
	std::cout << "SDL Reported System RAM: " << SDL_GetSystemRAM() << " MegaBytes" << std::endl;
	std::cout << "OpenMP Reported Processor Count: " << omp_get_num_procs() << std::endl;
	std::cout << "OpenMP Reported Thread Count: " << omp_get_max_threads() << std::endl;
	omp_set_num_threads(omp_get_max_threads());

	if (createSDLWindow() != 0)
	{
		std::cout << "WindowManager::createSDLWindow() Error: " << SDL_GetError() << std::endl;
		return -1;
	}
	else
	{
		if (initOpenGLContext() != 0)
		{
			std::cout << "WindowManager::initOpenGLContext() Error: " << SDL_GetError() << std::endl;
			return -1;
		}
	}
	return 0;
}

std::shared_ptr<SDL_Window> WindowManager::getWindow() const
{
	return m_SDL_Window;
}

int WindowManager::initSDLVideo()
{
	if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0)
	{
		std::cout << "SDL_InitSubSystem(SDL_INIT_VIDEO) Error: " << SDL_GetError() << std::endl;
		return -1;
	}
	return 0;
}

int WindowManager::createSDLWindow()
{
	if (initSDLVideo() != 0)
	{
		std::cout << "WindowManager::initSDLVideo() Error: " << SDL_GetError() << std::endl;
		return -1;
	}
	else
	{
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
		m_SDL_Window = std::shared_ptr<SDL_Window>(SDL_CreateWindow("CGT-Honours",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			1280, 720,
			SDL_WINDOW_OPENGL),
			[](SDL_Window* p_window)
		{
			SDL_DestroyWindow(p_window);
		});
		if (m_SDL_Window == nullptr)
		{
			std::cout << "SDL_CreateWindow() Error: " << SDL_GetError() << std::endl;
			return 1;
		}
		return 0;
	}
}

// credit - http://www.altdevblogaday.com/2011/06/23/improving-opengl-error-messages/
void FormatDebugOutputARB(char outStr[], size_t outStrSize, GLenum source, GLenum type, GLuint id, GLenum severity, const char *msg)
{
#ifdef _WIN64
#pragma warning( push )
#pragma warning( disable : 4996 )
#endif
	char sourceStr[32];
	const char *sourceFmt = "UNDEFINED(0x%04X)";
	switch (source)

	{
	case GL_DEBUG_SOURCE_API_ARB:
		sourceFmt = "API";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:
		sourceFmt = "WINDOW_SYSTEM";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB:
		sourceFmt = "SHADER_COMPILER";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:
		sourceFmt = "THIRD_PARTY";
		break;
	case GL_DEBUG_SOURCE_APPLICATION_ARB:
		sourceFmt = "APPLICATION";
		break;
	case GL_DEBUG_SOURCE_OTHER_ARB:
		sourceFmt = "OTHER";
		break;
	}

#ifdef _WIN64
	_snprintf(sourceStr, 32, sourceFmt, source);
#else
	snprintf(sourceStr, 32, sourceFmt, source);
#endif

	char typeStr[32];
	const char *typeFmt = "UNDEFINED(0x%04X)";
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR_ARB:
		typeFmt = "ERROR";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB:
		typeFmt = "DEPRECATED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:
		typeFmt = "UNDEFINED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_PORTABILITY_ARB:
		typeFmt = "PORTABILITY";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE_ARB:
		typeFmt = "PERFORMANCE";
		break;
	case GL_DEBUG_TYPE_OTHER_ARB:
		typeFmt = "OTHER";
		break;
	}

#ifdef _WIN64
	_snprintf(typeStr, 32, typeFmt, type);
#else
	snprintf(typeStr, 32, typeFmt, type);
#endif

	char severityStr[32];
	const char *severityFmt = "UNDEFINED";
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH_ARB:
		severityFmt = "HIGH";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM_ARB:
		severityFmt = "MEDIUM";
		break;
	case GL_DEBUG_SEVERITY_LOW_ARB:
		severityFmt = "LOW";
		break;
	}

#ifdef _WIN64
	_snprintf(severityStr, 32, severityFmt, severity);
	_snprintf(outStr, outStrSize, "OpenGL: %s [source=%s type=%s severity=%s id=%u]", msg, sourceStr, typeStr, severityStr, id);
#else
	snprintf(severityStr, 32, severityFmt, severity);
	snprintf(outStr, outStrSize, "OpenGL: %s [source=%s type=%s severity=%s id=%u]", msg, sourceStr, typeStr, severityStr, id);
#endif
#ifdef _WIN64
#pragma warning( pop )
#endif
}

void DebugCallbackARB(GLenum source, GLenum type, GLuint id, GLenum severity,
	GLsizei length, const GLchar *message, GLvoid *userParam)
{
	(void)length;
	FILE *outFile = (FILE*)userParam;
	char finalMessage[256];
	FormatDebugOutputARB(finalMessage, 256, source, type, id, severity, message);
	fprintf(outFile, "%s\n", finalMessage);
}

int WindowManager::initOpenGLContext()
{
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
#ifndef NDEBUG
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

	m_glcontext = SDL_GL_CreateContext(m_SDL_Window.get());

	if (!m_glcontext)
	{
		std::cout << "SDL_GL_CreateContext() Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	if (SDL_GL_MakeCurrent(m_SDL_Window.get(), m_glcontext) != 0) {
		std::cout << "SDL_GL_MakeCurrent() Error: " << SDL_GetError() << std::endl;
		return -1;
	}

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)   // glewInit failed, something is seriously wrong
	{
		std::cout << "glewInit() Error: " << glewGetErrorString(err) << std::endl;
		return 1;
	}

	std::cout << "Status: Using GLEW " << glewGetString(GLEW_VERSION) << std::endl;

	if (!GLEW_VERSION_4_4)
	{
		std::cout << "Error: Minimum OpenGL version 4.4 required!" << std::endl;
		return 1;
	}
	std::cout << "GL_VENDOR: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "GL_RENDERER: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "GL_VERSION: " << glGetString(GL_VERSION) << std::endl;
	std::cout << "GL_SHADING_LANGUAGE_VERSION: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	SDL_GL_SetSwapInterval(0);

#ifndef NDEBUG
	if (glewIsSupported("GL_ARB_debug_output"))
	{
		PFNGLDEBUGMESSAGECALLBACKARBPROC glDebugMessageCallbackARB = NULL;
		glDebugMessageCallbackARB = (PFNGLDEBUGMESSAGECALLBACKARBPROC)SDL_GL_GetProcAddress("glDebugMessageCallbackARB");

		if (glDebugMessageCallbackARB != NULL)
		{
			std::cout << "DEBUG OUTPUT SUPPORTED" << std::endl;

			glDebugMessageCallbackARB((GLDEBUGPROCARB)DebugCallbackARB, stderr);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
		}
	}
	else
	{
		std::cout << "DEBUG OUTPUT NOT SUPPORTED!" << std::endl;
		return 1;
	}
#endif
	return 0;
}


