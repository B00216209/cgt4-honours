#include "RendererManager.h"

RendererManager::RendererManager(std::shared_ptr<ServiceManager> p_serviceManager) : m_serviceManager(p_serviceManager), m_currentRendererIndex(0), 
m_VSIEnabled(false), m_GSIEnabled(false), m_GSDEnabled(false), m_DDCEnabled(false)
{
}

RendererManager::~RendererManager()
{
}

int RendererManager::setCurrentRenderer(const std::string& p_rendererName)
{
	for (size_t i = 0; i < m_rendererNames.size(); ++i)
	{
		if (m_rendererNames[i] == p_rendererName)
		{
			if (m_currentRendererPointer != nullptr) m_currentRendererPointer->cleanUpScene();
			m_currentRendererIndex = i;
			m_currentRendererPointer = m_rendererPointers[i];
			m_currentRendererPointer->refresh();
			m_currentRendererPointer->initScene(m_serviceManager->getSceneManager()->getCurrentScenePointer());
			return 0;
		}
	}
	return -1;
}

void RendererManager::goToNextRenderer()
{
	if (m_currentRendererIndex < m_rendererPointers.size() - 1)
	{
		m_currentRendererPointer->cleanUpScene();
		m_currentRendererIndex++;
		m_currentRendererPointer = m_rendererPointers[m_currentRendererIndex];
		m_currentRendererPointer->refresh();
		m_currentRendererPointer->initScene(m_serviceManager->getSceneManager()->getCurrentScenePointer());
		std::cout << "Changed Renderer from " << m_rendererNames[m_currentRendererIndex - 1] << " to " << m_rendererNames[m_currentRendererIndex] << std::endl;
	}
	else
	{
		m_currentRendererPointer->cleanUpScene();
		m_currentRendererIndex = 0;
		m_currentRendererPointer = m_rendererPointers[m_currentRendererIndex];
		m_currentRendererPointer->refresh();
		m_currentRendererPointer->initScene(m_serviceManager->getSceneManager()->getCurrentScenePointer());
		std::cout << "Changed Renderer from " << m_rendererNames[m_rendererPointers.size() - 1] << " to " << m_rendererNames[m_currentRendererIndex] << std::endl;
	}
}

void RendererManager::goToPreviousRenderer()
{
	if (m_currentRendererIndex > 0)
	{
		m_currentRendererPointer->cleanUpScene();
		m_currentRendererIndex--;
		m_currentRendererPointer = m_rendererPointers[m_currentRendererIndex];
		m_currentRendererPointer->refresh();
		m_currentRendererPointer->initScene(m_serviceManager->getSceneManager()->getCurrentScenePointer());
		std::cout << "Changed Renderer from " << m_rendererNames[m_currentRendererIndex + 1] << " to " << m_rendererNames[m_currentRendererIndex] << std::endl;
	}
	else
	{
		m_currentRendererPointer->cleanUpScene();
		m_currentRendererIndex = m_rendererPointers.size() - 1;
		m_currentRendererPointer = m_rendererPointers[m_currentRendererIndex];
		m_currentRendererPointer->refresh();
		m_currentRendererPointer->initScene(m_serviceManager->getSceneManager()->getCurrentScenePointer());
		std::cout << "Changed Renderer from " << m_rendererNames[0] << " to " << m_rendererNames[m_currentRendererIndex] << std::endl;
	}
}

int RendererManager::init()
{
	if (!glewIsSupported("GL_AMD_vertex_shader_viewport_index") && !glewIsSupported("GL_NV_viewport_array2"))
	{
		std::cout << "VSInstancedRendering DISABLED!: vertex shader viewport index not supported." << std::endl;
	}
	else
	{
		std::shared_ptr<VSInstancedRendering> rVSInstancedRendering = std::make_shared<VSInstancedRendering>(m_serviceManager);
		if (rVSInstancedRendering->init() != 0)
		{
			std::cout << "VSInstancedRendering::Init() Error! " << std::endl;
		}
		else
		{
			m_rendererPointers.push_back(rVSInstancedRendering);
			m_rendererNames.push_back("VSInstancedRendering");
			m_VSIEnabled = true;
			setCurrentRenderer("VSInstancedRendering");
		}		
	}
	
	std::shared_ptr<GSInstancedRendering> rGSInstancedRendering = std::make_shared<GSInstancedRendering>(m_serviceManager);
	if (rGSInstancedRendering->init() != 0)
	{
		std::cout << "GSInstancedRendering::Init() Error! " << std::endl;
	}
	else
	{
		m_rendererPointers.push_back(rGSInstancedRendering);
		m_rendererNames.push_back("GSInstancedRendering");
		m_GSIEnabled = true;
		setCurrentRenderer("GSInstancedRendering");
	}

	std::shared_ptr<GSDuplicatedGeometry> rGSDuplicatedGeometry = std::make_shared<GSDuplicatedGeometry>(m_serviceManager);
	if (rGSDuplicatedGeometry->init() != 0)
	{
		std::cout << "GSDuplicatedGeometry::Init() Error! " << std::endl;
	}
	else
	{
		m_rendererPointers.push_back(rGSDuplicatedGeometry);
		m_rendererNames.push_back("GSDuplicatedGeometry");
		m_GSDEnabled = true;
		setCurrentRenderer("GSDuplicatedGeometry");
	}

	std::shared_ptr<DuplicateCalls> rDuplicateCalls = std::make_shared<DuplicateCalls>(m_serviceManager);
	if (rDuplicateCalls->init() != 0)
	{
		std::cout << "DuplicateCalls::Init() Error! " << std::endl;
	}
	else
	{
		m_rendererPointers.push_back(rDuplicateCalls);
		m_rendererNames.push_back("DuplicateCalls");
		m_DDCEnabled = true;
		setCurrentRenderer("DuplicateCalls");
	}

	if (m_VSIEnabled || m_GSIEnabled || m_GSDEnabled || m_DDCEnabled) return 0;
	else return -1;
}

const std::shared_ptr<AbstractRenderer> RendererManager::getCurrentRendererPointer() const
{
	return m_currentRendererPointer;
}

bool RendererManager::getVSIEnabled() const
{
	return m_VSIEnabled;
}

bool RendererManager::getGSIEnabled() const
{
	return m_GSIEnabled;
}

bool RendererManager::getGSDEnabled() const
{
	return m_GSDEnabled;
}

bool RendererManager::getDDCEnabled() const
{
	return m_DDCEnabled;
}
