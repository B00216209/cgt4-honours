#include "GSDuplicatedGeometry.h"


GSDuplicatedGeometry::GSDuplicatedGeometry(std::shared_ptr<ServiceManager> p_serviceManager) : AbstractRenderer(p_serviceManager)
{
}

GSDuplicatedGeometry::~GSDuplicatedGeometry()
{
	if (m_vertexBufferObjectVector.size() != 0) cleanUpScene();
}

int GSDuplicatedGeometry::initScene(const std::shared_ptr<AbstractScene>& p_currentScene)
{
	if (m_vertexBufferObjectVector.size() != 0) cleanUpScene();
	sceneSetup(1, p_currentScene);
	return 0;
}

int GSDuplicatedGeometry::cleanUpScene()
{
	sceneCleanUp();
	return 0;
}


int GSDuplicatedGeometry::init()
{
	if (initShaders("shaders/GSDuplicatedGeometry.vert", "shaders/GSDuplicatedGeometry.geo", "shaders/unlit_Texture.frag",
		"shaders/GSDuplicatedGeometry.vert", "shaders/GSDuplicatedGeometry.geo", "shaders/lit_LambertianDiffuse.frag",
		"shaders/GSDuplicatedGeometry.vert", "shaders/GSDuplicatedGeometry.geo", "shaders/lit_PBR.frag") == -1)
	{
		return -1;
	}

	initViewportArray();
	initViewandProjectionMatrices();

	return 0;
}

void GSDuplicatedGeometry::refresh()
{
	glClearColor(0.2f, 0.2f, 0.7f, 1.0f);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDisable(GL_BLEND);
	glDisable(GL_SCISSOR_TEST);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_FALSE);
	glDepthMask(GL_TRUE);

	setCurrentShader("HighComplexity");
	glViewportArrayv(0, 2, m_viewPortArray.data());
}

void GSDuplicatedGeometry::render()
{
	renderViewportArray();
}