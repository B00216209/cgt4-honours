#include "AbstractRenderer.h"

AbstractRenderer::~AbstractRenderer()
{
}

int AbstractRenderer::setCurrentShader(const std::string& p_key)
{
	std::unordered_map<std::string, GLuint>::const_iterator got = m_shaderPrograms.find(p_key);
	if (got != m_shaderPrograms.end())
	{
		std::cout << "Changed Shader from " << m_currentShaderKey << " to " << p_key << std::endl;
		m_currentShaderKey = p_key;
		glUseProgram(m_shaderPrograms.at(p_key));
	
		return 0;
	}
	else
	{
		std::cout << "setCurrentShader() Error!: p_key not found" << std::endl;
		return 1;
	}
}

void AbstractRenderer::sceneCleanUp()
{
	glDeleteQueries(1, &m_GPUTimeQuery1);
	glDeleteQueries(1, &m_GPUTimeQuery0);

	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);

	glDeleteTextures(1, &m_textureArray);

	glDeleteBuffers(1, &m_elementArrayBufferObject);
	glDeleteBuffers(1, &m_vertexBufferObjectVector[3]);
	glDeleteBuffers(1, &m_vertexBufferObjectVector[2]);
	glDeleteBuffers(1, &m_vertexBufferObjectVector[1]);
	glDeleteBuffers(1, &m_vertexBufferObjectVector[0]);
	glDeleteBuffers(1, &m_vertexArray);
	glDeleteBuffers(1, &m_drawIndirectBuffer);
	glDeleteBuffers(1, &m_modelMatrixBuffer);

	if (m_syncObject0 != NULL)
	{
		glDeleteSync(m_syncObject0);
		m_syncObject0 = NULL;
	}
	if (m_syncObject1 != NULL)
	{
		glDeleteSync(m_syncObject1);
		m_syncObject1 = NULL;
	}
	if (m_syncObject2 != NULL)
	{
		glDeleteSync(m_syncObject2);
		m_syncObject2 = NULL;
	}

	m_GPUModelMatrixBufferPtr = nullptr;

	m_modelMatrixBufferOffset = 0;
	m_modelMatrixBufferSize = 0;
	m_textureArray = 0;
	m_elementArrayBufferObject = 0;
	m_vertexArray = 0;
	m_drawIndirectBuffer = 0;

	m_renderableObjects.clear();
	m_perDrawVariables.clear();
	m_perMeshVariables.clear();
	m_drawIndirectCommandVector.clear();
	m_vertexBufferObjectVector.clear();
}

void AbstractRenderer::fillCommandandPerDrawBuffers(GLuint p_instanceCount, const std::shared_ptr<AbstractScene>& p_currentScene)
{
	const std::vector<GLuint>& p_perMeshInstanceCountVector = p_currentScene->getperMeshInstanceCountVector();
	const std::vector<GLuint>& p_renderablObjectTextureIDVector = p_currentScene->getRenderableObjectTextureIDVector();
	const std::vector<glm::vec4>& p_positionVector = p_currentScene->getPositionVector();
	const std::vector<glm::fquat>& p_orientationVector = p_currentScene->getOrientationVector();

	DrawElementsIndirectCommand cmd;
	GLuint renderableCount = 0;
	glm::uvec2 drawVariables;

	m_renderableObjects.reserve(p_currentScene->getRenderableObjectCount());
	m_perDrawVariables.reserve(p_currentScene->getRenderableObjectCount());
	m_drawIndirectCommandVector.reserve(p_currentScene->getRenderableObjectCount());

	for (size_t meshIndex = 0; meshIndex < p_perMeshInstanceCountVector.size(); ++meshIndex)
	{
		for (size_t instanceIndex = 0; instanceIndex < p_perMeshInstanceCountVector[meshIndex]; ++instanceIndex)
		{
			cmd.count = m_perMeshVariables[meshIndex].x;
			cmd.instanceCount = p_instanceCount;
			cmd.firstIndex = m_perMeshVariables[meshIndex].y;
			cmd.baseVertex = 0;
			cmd.baseInstance = renderableCount;
			drawVariables.x = cmd.baseInstance;
			drawVariables.y = p_renderablObjectTextureIDVector[renderableCount] + 1;
			m_perDrawVariables.push_back(drawVariables);
			m_drawIndirectCommandVector.push_back(cmd);
			m_renderableObjects.push_back(RenderableObject(cmd.count,
				cmd.firstIndex,
				cmd.instanceCount,
				p_renderablObjectTextureIDVector[renderableCount] + 1,
				p_positionVector[renderableCount],
				p_orientationVector[renderableCount]));
			renderableCount++;
		}
	}
	glBufferData(GL_DRAW_INDIRECT_BUFFER, m_drawIndirectCommandVector.size() * sizeof(DrawElementsIndirectCommand), m_drawIndirectCommandVector.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObjectVector[3]);
	glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(GLuint) * m_perDrawVariables.size(), m_perDrawVariables.data(), GL_STATIC_DRAW);
}

void AbstractRenderer::sceneSetup(GLuint p_instanceCount, const std::shared_ptr<AbstractScene>& p_currentScene)
{
	m_vertexBufferObjectVector.reserve(4);
	m_vertexBufferObjectVector.assign(4, 0);

	initTimerQueries();
	initModelMatrixBuffer(p_currentScene);
	initVAOandBuffers(p_instanceCount, p_currentScene);
	initTextureArray(p_currentScene);

	fillTextureArray(p_currentScene);
	fillVAObuffers(p_currentScene);
	fillCommandandPerDrawBuffers(p_instanceCount, p_currentScene);
}

GLuint AbstractRenderer::initGLVertexAttribBuffer(GLenum p_dataType, GLint p_dataTypeCount, GLsizeiptr p_dataSizeInBytes, GLuint p_attribID, GLuint p_attribDivisor, GLsizei p_stride, GLenum p_bufferUse)
{
	GLuint ID = initGLBuffer(p_dataSizeInBytes, GL_ARRAY_BUFFER, p_bufferUse);
	glVertexAttribPointer(p_attribID, p_dataTypeCount, p_dataType, GL_FALSE, p_stride, 0);
	glVertexAttribDivisor(p_attribID, p_attribDivisor);
	glEnableVertexAttribArray(p_attribID);
	return ID;
}

GLuint AbstractRenderer::initGLBuffer(GLsizeiptr p_dataSizeInBytes, GLenum p_bufferType, GLenum p_bufferUse)
{
	GLuint ID;
	glGenBuffers(1, &ID);
	glBindBuffer(p_bufferType, ID);
	glBufferData(p_bufferType, p_dataSizeInBytes, NULL, p_bufferUse);
	return ID;
}

void AbstractRenderer::initVAOandBuffers(GLuint p_instanceCount, const std::shared_ptr<AbstractScene>& p_currentScene)
{
	GLuint vertexCount = p_currentScene->getVertexCount();
	GLuint renderableObjectCount = p_currentScene->getRenderableObjectCount();
	GLuint indexCount = p_currentScene->getIndexCount();

	glGenBuffers(1, &m_drawIndirectBuffer);
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_drawIndirectBuffer);

	glGenVertexArrays(1, &m_vertexArray);
	glBindVertexArray(m_vertexArray);

	// Positions
	m_vertexBufferObjectVector[0] = initGLVertexAttribBuffer(GL_FLOAT, 3, 3 * sizeof(GLfloat) * vertexCount, 0, 0, 0, GL_STATIC_DRAW);
	// Normals
	m_vertexBufferObjectVector[1] = initGLVertexAttribBuffer(GL_FLOAT, 3, 3 * sizeof(GLfloat) * vertexCount, 1, 0, 0, GL_STATIC_DRAW);
	// UVs
	m_vertexBufferObjectVector[2] = initGLVertexAttribBuffer(GL_FLOAT, 2, 2 * sizeof(GLfloat) * vertexCount, 2, 0, 0, GL_STATIC_DRAW);
	// Per-instance variable, shader draw parameters workaround! (When I set the buffer dataType to GL_UNSIGNED_INT here the modelMatrix lookup breaks, TODO: figure this out)
	m_vertexBufferObjectVector[3] = initGLVertexAttribBuffer(GL_FLOAT, 2, sizeof(glm::uvec2) * renderableObjectCount, 3, p_instanceCount, 0, GL_STATIC_DRAW);
	// Indices
	m_elementArrayBufferObject = initGLBuffer(sizeof(GLuint) * indexCount, GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW);
}

void AbstractRenderer::initModelMatrixBuffer(const std::shared_ptr<AbstractScene>& p_currentScene)
{
	m_modelMatrixVector = p_currentScene->getModelMatrixVector();

	// ModelMatrix buffer, tripple buffered
	glGenBuffers(1, &m_modelMatrixBuffer);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_modelMatrixBuffer);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_modelMatrixBuffer);
	const GLbitfield mapFlags = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
	const GLbitfield createFlags = mapFlags | GL_DYNAMIC_STORAGE_BIT;
	m_modelMatrixBufferSize = m_modelMatrixVector.size()*sizeof(glm::mat4);
	GLint bufferAlignment;
	glGetIntegerv(GL_MIN_MAP_BUFFER_ALIGNMENT, &bufferAlignment);
	if (bufferAlignment < 16) bufferAlignment = 16; // Minimum alignment of 16 bytes to allow for future SSE2 use.
	m_modelMatrixBufferAlginedSize = static_cast<std::size_t>(ceil((double)m_modelMatrixBufferSize / (double)bufferAlignment) * bufferAlignment); // This is to ensure that the buffer is aligned correctly
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, m_modelMatrixBufferAlginedSize * 3, 0, createFlags);
	m_GPUModelMatrixBufferPtr = glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, m_modelMatrixBufferAlginedSize * 3, mapFlags);
}

void AbstractRenderer::initTextureArray(const std::shared_ptr<AbstractScene>& p_currentScene)
{
	glGenTextures(1, &m_textureArray);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, m_textureArray);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	GLuint textureDepth = p_currentScene->getTextureCount() + 1;
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, 2, 2, textureDepth);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

int AbstractRenderer::fillTextureArray(const std::shared_ptr<AbstractScene>& p_currentScene)
{
	const std::vector<std::string> textureFilenameVector = p_currentScene->getTextureFilenameVector();
	GLuint textureCount = 0;
	// Insert a default texture at layer 0 to aid debugging.
	int imageWidth, imageHeight, imageChannels;
	unsigned char* rawImage;
	rawImage = SOIL_load_image("textures/DebugTexture.BMP", &imageWidth, &imageHeight, &imageChannels, SOIL_LOAD_RGBA);
	if (rawImage == 0x00)
	{
		std::cout << "SOIL_load_image() Error: " << SOIL_last_result() << std::endl;
		return -1;
	}
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, imageWidth, imageHeight, 1, GL_RGBA, GL_UNSIGNED_BYTE, rawImage);
	delete[] rawImage;
	textureCount++;
	// insert all textures from the scene into the array starting at index 1
	for (size_t textureIndex = 0; textureIndex < textureFilenameVector.size(); ++textureIndex)
	{
		unsigned char* rawImage = SOIL_load_image(textureFilenameVector[textureIndex].c_str(), &imageWidth, &imageHeight, &imageChannels, SOIL_LOAD_RGBA);
		if (rawImage == 0x00)
		{
			std::cout << "SOIL_load_image() Error: " << SOIL_last_result() << std::endl;
			continue;
		}
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, static_cast<GLint>(textureIndex)+1, imageWidth, imageHeight, 1, GL_RGBA, GL_UNSIGNED_BYTE, rawImage);
		delete[] rawImage;
		textureCount++;
	}
	return 0;
}

void AbstractRenderer::fillVAObuffers(const std::shared_ptr<AbstractScene>& p_currentScene)
{
	aiMesh* currentMesh;
	std::vector<GLuint> indexVector;
	GLuint vertexAttributeOffset = 0;
	GLuint vertexIndexOffset = 0;
	const std::vector<const aiScene*> sceneVector = p_currentScene->getSceneVector();

	for (size_t sceneIndex = 0; sceneIndex < sceneVector.size(); ++sceneIndex)
	{
		currentMesh = sceneVector[sceneIndex]->mMeshes[0];
		indexVector.clear();
		indexVector.reserve(currentMesh->mNumFaces * 3);
		aiFace face;
		for (uint32_t faceIndex = 0; faceIndex < currentMesh->mNumFaces; ++faceIndex)
		{
			face = currentMesh->mFaces[faceIndex];
			indexVector.push_back(face.mIndices[0] + vertexAttributeOffset);
			indexVector.push_back(face.mIndices[1] + vertexAttributeOffset);
			indexVector.push_back(face.mIndices[2] + vertexAttributeOffset);
		}
		// Append this mesh's atrributes
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObjectVector[0]);
		glBufferSubData(GL_ARRAY_BUFFER, 3 * sizeof(GLfloat)*vertexAttributeOffset, 3 * sizeof(GLfloat)  *currentMesh->mNumVertices, currentMesh->mVertices);

		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObjectVector[1]);
		glBufferSubData(GL_ARRAY_BUFFER, 3 * sizeof(GLfloat)*vertexAttributeOffset, 3 * sizeof(GLfloat) * currentMesh->mNumVertices, currentMesh->mNormals);

		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObjectVector[2]);
		// aiScene contains UV's in a vec3 format, this is to pack it tighter without the 3rd component
		std::vector<float> uvs;
		uvs.reserve(2 * currentMesh->mNumVertices);
		aiVector3D* uvChannel = currentMesh->mTextureCoords[0];
		\
			for (size_t i = 0; i < currentMesh->mNumVertices; ++i)
			{
				uvs.push_back(uvChannel[i].x);
				uvs.push_back(uvChannel[i].y);
			}
		glBufferSubData(GL_ARRAY_BUFFER, 2 * sizeof(GLfloat)*vertexAttributeOffset, 2 * sizeof(GLfloat) * currentMesh->mNumVertices, uvs.data());
		// Append this mesh's indices
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*vertexIndexOffset, sizeof(GLuint) * indexVector.size(), indexVector.data());

		m_perMeshVariables.push_back(glm::uvec2(currentMesh->mNumFaces * 3, vertexIndexOffset));
		vertexAttributeOffset += currentMesh->mNumVertices;
		vertexIndexOffset += currentMesh->mNumFaces * 3;
	}
}

void AbstractRenderer::waitforSync(GLsync* p_syncObject)
{
	GLsync syncObject = *p_syncObject;
	if (syncObject != NULL)
	{
		GLenum waitRet = GL_UNSIGNALED;
		while (waitRet != GL_ALREADY_SIGNALED && waitRet != GL_CONDITION_SATISFIED)
		{
			waitRet = glClientWaitSync(syncObject, GL_SYNC_FLUSH_COMMANDS_BIT, 1000000000);
		}
		glDeleteSync(syncObject);
	}
}

void AbstractRenderer::initTimerQueries()
{
	glGenQueries(1, &m_GPUTimeQuery0);
	glGenQueries(1, &m_GPUTimeQuery1);
	m_GPUTimerQueries.push_back(m_GPUTimeQuery0);
	m_GPUTimerQueries.push_back(m_GPUTimeQuery1);
	glBeginQuery(GL_TIME_ELAPSED, m_GPUTimeQuery0);
	glEndQuery(GL_TIME_ELAPSED);
	glBeginQuery(GL_TIME_ELAPSED, m_GPUTimeQuery1);
	glEndQuery(GL_TIME_ELAPSED);
}

void AbstractRenderer::update()
{
	auto startTick = SDL_GetPerformanceCounter();

	const int64_t iterations = m_renderableObjects.size();
	glm::fquat newOrientation;
	const glm::vec3 rotationAxis = glm::vec3(0.0f, 1.0f, 0.0f);
	const float rotationAmount = 0.017f;
	glm::fquat rotationQuat = glm::rotate(glm::fquat(), rotationAmount, rotationAxis);

#pragma omp parallel for
	for (int64_t i = 0; i < iterations; i++)
	{
		m_renderableObjects[i].setOrientation(rotationQuat * m_renderableObjects[i].getOrientation());
		m_modelMatrixVector[i] = m_renderableObjects[i].getModelMatrix();
	}

	if (m_modelMatrixBufferOffset == 0)
	{
		waitforSync(&m_syncObject0);
	}
	else if (m_modelMatrixBufferOffset == m_modelMatrixBufferAlginedSize)
	{
		waitforSync(&m_syncObject1);
	}
	else
	{
		waitforSync(&m_syncObject2);
	}

	void* dst = (unsigned char*)m_GPUModelMatrixBufferPtr + m_modelMatrixBufferOffset;
	memcpy(dst, m_modelMatrixVector.data(), m_modelMatrixBufferSize);
	glBindBufferRange(GL_SHADER_STORAGE_BUFFER, 0, m_modelMatrixBuffer, m_modelMatrixBufferOffset, m_modelMatrixBufferSize);

	auto endTick = SDL_GetPerformanceCounter();

	uint64_t tickDelta = endTick - startTick;
	uint64_t tickRate = SDL_GetPerformanceFrequency();
	double tickDeltaTime = static_cast<double>(tickDelta) / static_cast<double>(tickRate);
	m_updateCallCPUDurationInNanoseconds = tickDeltaTime * 1000000000;
}

void AbstractRenderer::renderViewportArray()
{
	glBeginQuery(GL_TIME_ELAPSED, m_GPUTimerQueries[m_currentTimerQueryIndex]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, nullptr, (GLuint)m_drawIndirectCommandVector.size(), 0);

	if (m_modelMatrixBufferOffset == 0)
	{
		m_syncObject0 = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	}
	else if (m_modelMatrixBufferOffset == m_modelMatrixBufferAlginedSize)
	{
		m_syncObject1 = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	}
	else
	{
		m_syncObject2 = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	}

	m_modelMatrixBufferOffset = (m_modelMatrixBufferOffset + m_modelMatrixBufferAlginedSize) % (m_modelMatrixBufferAlginedSize * 3);

	glEndQuery(GL_TIME_ELAPSED);
	m_currentTimerQueryIndex = (m_currentTimerQueryIndex + 1) % 2;
	glGetQueryObjecti64v(m_GPUTimerQueries[m_currentTimerQueryIndex], GL_QUERY_RESULT, &m_renderCallGPUDurationInNanoSeconds);

	SDL_GL_SwapWindow(m_serviceManager->getWindowManager()->getWindow().get());
}

void AbstractRenderer::renderMultipleViewports()
{
	glBeginQuery(GL_TIME_ELAPSED, m_GPUTimerQueries[m_currentTimerQueryIndex]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (size_t currentVP = 0; currentVP < 2; ++currentVP)
	{
		glViewport(m_viewPorts[currentVP].x, m_viewPorts[currentVP].y, m_viewPorts[currentVP].width, m_viewPorts[currentVP].height);
		glUniform1i(m_viewportIndexLocations.at(m_currentShaderKey), static_cast<GLint>(currentVP));
		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, nullptr, (GLuint)m_drawIndirectCommandVector.size(), 0);
	}

	if (m_modelMatrixBufferOffset == 0)
	{
		m_syncObject0 = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	}
	else if (m_modelMatrixBufferOffset == m_modelMatrixBufferAlginedSize)
	{
		m_syncObject1 = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	}
	else
	{
		m_syncObject2 = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	}

	m_modelMatrixBufferOffset = (m_modelMatrixBufferOffset + m_modelMatrixBufferAlginedSize) % (m_modelMatrixBufferAlginedSize * 3);

	glEndQuery(GL_TIME_ELAPSED);
	m_currentTimerQueryIndex = (m_currentTimerQueryIndex + 1) % 2;
	glGetQueryObjecti64v(m_GPUTimerQueries[m_currentTimerQueryIndex], GL_QUERY_RESULT, &m_renderCallGPUDurationInNanoSeconds);

	SDL_GL_SwapWindow(m_serviceManager->getWindowManager()->getWindow().get());
}

GLint AbstractRenderer::initShaders(
	const std::string& p_lowVert, const std::string& p_lowGeo, const std::string& p_lowFrag,
	const std::string& p_midVert, const std::string& p_midGeo, const std::string& p_midFrag,
	const std::string& p_highVert, const std::string& p_highGeo, const std::string& p_highFrag)
{
	GLint lowCompReturnVal = initShader("LowComplexity", p_lowVert, p_lowGeo, p_lowFrag);
	GLint midCompReturnVal = initShader("MidComplexity", p_midVert, p_midGeo, p_midFrag);
	GLint highCompReturnVal = initShader("HighComplexity", p_highVert, p_highGeo, p_highFrag);

	if (lowCompReturnVal == -1 || midCompReturnVal == -1 || highCompReturnVal == -1)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

GLint AbstractRenderer::initShader(const std::string& p_key, const std::string& p_vert, const std::string& p_geo, const std::string& p_frag)
{
	GLint tempShader;
	if (p_geo == "")
	{
		tempShader = m_serviceManager->getShaderManager()->loadGLShader(p_vert, p_frag);

	}
	else
	{
		tempShader = m_serviceManager->getShaderManager()->loadGLShader(p_vert, p_geo, p_frag);
	}
	if (tempShader != -1)
	{
		std::pair<std::string, GLuint> shaderPair(p_key, tempShader);
		m_shaderPrograms.insert(shaderPair);
		return tempShader;
	}
	else
	{
		std::cout << "initShader() Error!: " << p_key << " Shader did not compile correctly" << std::endl;
		return -1;
	}
}

void AbstractRenderer::initViewportArray()
{
	const GLsizei halfWidth = 640;
	const GLsizei windowHeight = 720;
	m_viewPortArray.reserve(8);
	m_viewPortArray.push_back(0.0f);
	m_viewPortArray.push_back(0.0f);
	m_viewPortArray.push_back(static_cast<GLfloat>(halfWidth));
	m_viewPortArray.push_back(static_cast<GLfloat>(windowHeight));
	m_viewPortArray.push_back(static_cast<GLfloat>(halfWidth));
	m_viewPortArray.push_back(0.0f);
	m_viewPortArray.push_back(static_cast<GLfloat>(halfWidth));
	m_viewPortArray.push_back(static_cast<GLfloat>(windowHeight));
}

void AbstractRenderer::initMultipleViewports()
{
	const GLsizei halfWidth = 640;
	const GLsizei windowHeight = 720;
	m_viewPorts.reserve(8);
	viewPort temp = viewPort(0, 0, halfWidth, windowHeight);
	m_viewPorts.push_back(temp);
	temp = viewPort(halfWidth, 0, halfWidth, windowHeight);
	m_viewPorts.push_back(temp);

	std::pair<std::string, GLint> tempPair = std::pair<std::string, GLint>("LowComplexity", glGetUniformLocation(m_shaderPrograms.at("LowComplexity"), "viewportIndex"));
	m_viewportIndexLocations.insert(tempPair);

	tempPair = std::pair<std::string, GLint>("MidComplexity", glGetUniformLocation(m_shaderPrograms.at("MidComplexity"), "viewportIndex"));
	m_viewportIndexLocations.insert(tempPair);

	tempPair = std::pair<std::string, GLint>("HighComplexity", glGetUniformLocation(m_shaderPrograms.at("HighComplexity"), "viewportIndex"));
	m_viewportIndexLocations.insert(tempPair);
}

void AbstractRenderer::initViewandProjectionMatrices()
{
	m_projMatrices.push_back(glm::perspective(70.0f, 640.0f / 720.0f, 1.0f, 300.0f));
	m_projMatrices.push_back(glm::perspective(70.0f, 640.0f / 720.0f, 1.0f, 300.0f));
	m_viewMatrices.push_back(glm::lookAt(glm::vec3(-80.0f, 2.0f, 80.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
	m_viewMatrices.push_back(glm::lookAt(glm::vec3(-80.0f, -2.0f, 80.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

	setCurrentShader("LowComplexity");
	glUniformMatrix4fv(glGetUniformLocation(m_shaderPrograms.at("LowComplexity"), "projectionMatrices"), 2, GL_FALSE, (GLfloat*)m_projMatrices.data());
	glUniformMatrix4fv(glGetUniformLocation(m_shaderPrograms.at("LowComplexity"), "viewMatrices"), 2, GL_FALSE, (GLfloat*)m_viewMatrices.data());

	setCurrentShader("MidComplexity");
	glUniformMatrix4fv(glGetUniformLocation(m_shaderPrograms.at("MidComplexity"), "projectionMatrices"), 2, GL_FALSE, (GLfloat*)m_projMatrices.data());
	glUniformMatrix4fv(glGetUniformLocation(m_shaderPrograms.at("MidComplexity"), "viewMatrices"), 2, GL_FALSE, (GLfloat*)m_viewMatrices.data());

	setCurrentShader("HighComplexity");
	glUniformMatrix4fv(glGetUniformLocation(m_shaderPrograms.at("HighComplexity"), "projectionMatrices"), 2, GL_FALSE, (GLfloat*)m_projMatrices.data());
	glUniformMatrix4fv(glGetUniformLocation(m_shaderPrograms.at("HighComplexity"), "viewMatrices"), 2, GL_FALSE, (GLfloat*)m_viewMatrices.data());
}