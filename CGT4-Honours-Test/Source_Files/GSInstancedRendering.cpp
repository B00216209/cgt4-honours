#include "GSInstancedRendering.h"


GSInstancedRendering::GSInstancedRendering(std::shared_ptr<ServiceManager> p_serviceManager) : AbstractRenderer(p_serviceManager)
{
}

GSInstancedRendering::~GSInstancedRendering()
{
	if (m_vertexBufferObjectVector.size() != 0) cleanUpScene();
}

int GSInstancedRendering::initScene(const std::shared_ptr<AbstractScene>& p_currentScene)
{
	if (m_vertexBufferObjectVector.size() != 0) cleanUpScene();
	sceneSetup(2, p_currentScene);
	return 0;
}

int GSInstancedRendering::cleanUpScene()
{
	sceneCleanUp();
	return 0;
}


int GSInstancedRendering::init()
{
	if (initShaders("shaders/GSInstancedRendering.vert", "shaders/GSInstancedRendering.geo", "shaders/unlit_Texture.frag",
		"shaders/GSInstancedRendering.vert", "shaders/GSInstancedRendering.geo", "shaders/lit_LambertianDiffuse.frag",
		"shaders/GSInstancedRendering.vert", "shaders/GSInstancedRendering.geo", "shaders/lit_PBR.frag") == -1)
	{
		return -1;
	}

	initViewportArray();
	initViewandProjectionMatrices();

	return 0;
}

void GSInstancedRendering::refresh()
{
	glClearColor(0.7f, 0.2f, 0.2f, 1.0f);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDisable(GL_BLEND);
	glDisable(GL_SCISSOR_TEST);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_FALSE);
	glDepthMask(GL_TRUE);

	setCurrentShader("HighComplexity");
	glViewportArrayv(0, 2, m_viewPortArray.data());
}

void GSInstancedRendering::render()
{
	renderViewportArray();
}