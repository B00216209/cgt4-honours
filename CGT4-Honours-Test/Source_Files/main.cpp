#include <memory>
#include "Application.h"
#include "argvparser.h"

// http://stackoverflow.com/a/12654801
#if defined(_MSC_VER)
#define ALIGNED_(x) __declspec(align(x))
#else
#if defined(__GNUC__)
#define ALIGNED_(x) __attribute__ ((aligned(x)))
#endif
#endif
#define _ALIGNED_TYPE(t,x) typedef t ALIGNED_(x)

int main(int argc, char* argv[])
{
	bool isBenchmarkMode = false;
	CommandLineProcessing::ArgvParser parser;
	parser.setIntroductoryDescription("B00216209 - Computer Games Technology Honours Project \nGPU Acceleration of Stereographic Rendering with OpenGL 4.4");
	parser.setHelpOption("h", "help", "Prints the help page");
	parser.addErrorCode(0, "Sucess");
	parser.addErrorCode(1, "Error");
	parser.defineOption("benchmark", "Enables benchmarking mode", CommandLineProcessing::ArgvParser::NoOptionAttribute);

	int result = parser.parse(argc, argv);
	if (result != CommandLineProcessing::ArgvParser::NoParserError){
		std::cout << parser.parseErrorDescription(result);
		exit(1);
	}
	else{
		if (parser.foundOption("benchmark")){
			isBenchmarkMode = true;
		}
	}

	Application m_application = Application();
	if (m_application.init() != 0)
	{
		std::cout << "Application::init() Error: " << std::endl;
		return -1;
	}
	else
	{
		if (isBenchmarkMode == true)
		{
			return m_application.benchmark();
		}
		else
		{
			return m_application.gameLoop();
		}
	}
}
