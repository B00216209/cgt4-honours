#include "Application.h"

Application::Application() : m_serviceManager(std::make_shared<ServiceManager>())
{
}

Application::~Application()
{
	closeSDL();
}

int Application::init()
{
	if (initSDL() != 0)
	{
		std::cout << "Application::initSDL() Error! " << std::endl;
		return -1;
	}

	std::shared_ptr<WindowManager> tempWM = std::make_shared<WindowManager>(m_serviceManager);
	if (tempWM->init() != 0)
	{
		std::cout << "WindowManager::Init() Error! " << std::endl;
		return -1;
	}
	m_serviceManager->setWindowManager(tempWM);

	std::shared_ptr<SceneManager> tempSM = std::make_shared<SceneManager>(m_serviceManager);
	if (tempSM->init() != 0)
	{
		std::cout << "SceneManager::Init() Error! " << std::endl;
		return -1;
	}
	m_serviceManager->setSceneManager(tempSM);

	std::shared_ptr<ShaderManager> tempSHM = std::make_shared<ShaderManager>(m_serviceManager);
	m_serviceManager->setShaderManager(tempSHM);

	std::shared_ptr<RendererManager> tempRM = std::make_shared<RendererManager>(m_serviceManager);
	if (tempRM->init() != 0)
	{
		std::cout << "RendererManager::Init() Error! " << std::endl;
		return -1;
	}
	m_serviceManager->setRendererManager(tempRM);

	return 0;
}

int Application::initSDL()
{
	if (SDL_Init(0) != 0)
	{
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return -1;
	}
	return 0;
}

void Application::closeSDL()
{
	SDL_Quit();
}

int Application::gameLoop()
{
	SDL_Event SDLEvent;
	bool isRunning = true;
	while (isRunning)
	{
		std::shared_ptr<AbstractRenderer> currentRenderer = m_serviceManager->getRendererManager()->getCurrentRendererPointer();
		while (SDL_PollEvent(&SDLEvent))
		{
			switch (SDLEvent.type)
			{
			case SDL_WINDOWEVENT:
				switch (SDLEvent.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE:
					isRunning = false;
					break;
				}
				break;
			case SDL_QUIT:
				isRunning = false;
				break;
			case SDL_KEYDOWN:
				switch (SDLEvent.key.keysym.sym)
				{
				case SDLK_F4:
					if (SDLEvent.key.keysym.mod == KMOD_LALT || SDLEvent.key.keysym.mod == KMOD_RALT)
					{
						isRunning = false;
					}
					break;
				case SDLK_ESCAPE:
					isRunning = false;
					break;
				}
				break;
			case SDL_KEYUP:
				switch (SDLEvent.key.keysym.sym)
				{
				case SDLK_f:
					printf("GPU render time in nanoseconds: %lu\n", currentRenderer->m_renderCallGPUDurationInNanoSeconds);
					printf("CPU update time in nanoseconds: %lu\n", currentRenderer->m_updateCallCPUDurationInNanoseconds);
					break;
				case SDLK_s:
					m_serviceManager->getSceneManager()->goToPreviousScene();
					break;
				case SDLK_d:
					m_serviceManager->getSceneManager()->goToNextScene();
					break;
				case SDLK_w:
					m_serviceManager->getRendererManager()->goToPreviousRenderer();
					break;
				case SDLK_e:
					m_serviceManager->getRendererManager()->goToNextRenderer();
					break;
				case SDLK_1:
					currentRenderer->setCurrentShader("LowComplexity");
					break;
				case SDLK_2:
					currentRenderer->setCurrentShader("MidComplexity");
					break;
				case SDLK_3:
					currentRenderer->setCurrentShader("HighComplexity");
					break;
				}
				break;
			}
		}
		currentRenderer = m_serviceManager->getRendererManager()->getCurrentRendererPointer();
		currentRenderer->update();
		currentRenderer->render();
	}
	return 0;
}

int Application::benchmark()
{
	std::string gpuName = (const char*)glGetString(GL_RENDERER);
	std::string OS = SDL_GetPlatform();
	Poco::UUIDGenerator& generator = Poco::UUIDGenerator::defaultGenerator();
	Poco::UUID uuid1(generator.createOne());
	std::string uuids = uuid1.toString();
	char eraser[] = "-";
	for (size_t i = 0; i < uuids.length(); ++i)
	{
		uuids.erase(std::remove(uuids.begin(), uuids.end(), eraser[0]), uuids.end());
	}
	
	std::vector<BenchmarkData> benchmarkDataVector(15);
	char shaderType = 0;
	char objectCount = 0;
	for (size_t i = 0; i < 15; ++i)
	{
		benchmarkDataVector[i].m_gpuName = gpuName;
		benchmarkDataVector[i].m_UUID = uuids;
		benchmarkDataVector[i].m_OS = OS;
	}
	std::string userUUID = "http://stereorenderingbenchmark.appspot.com/visualise?userUUID=" + uuids;
	SDL_SetClipboardText(userUUID.c_str());
		
	std::shared_ptr<RendererManager> rendererManager = m_serviceManager->getRendererManager();
		
	if (rendererManager->getVSIEnabled())
	{
		benchmarkLoopVSI(benchmarkDataVector);
	}

	if (rendererManager->getGSIEnabled())
	{
		benchmarkLoopGSI(benchmarkDataVector);
	}

	if (rendererManager->getGSDEnabled())
	{
		benchmarkLoopGSD(benchmarkDataVector);
	}

	if (rendererManager->getDDCEnabled())
	{
		benchmarkLoopDDC(benchmarkDataVector);
	}
	
	for (BenchmarkData benchmarkData : benchmarkDataVector)
	{
		submitBenchmarkData(benchmarkData);
	}
	return 0;
}

void Application::benchmarkLoopVSI(std::vector<BenchmarkData>& p_benchmarkDataVector)
{
	std::shared_ptr<RendererManager> rendererManager = m_serviceManager->getRendererManager();
	std::shared_ptr<SceneManager> sceneManager = m_serviceManager->getSceneManager();
	std::shared_ptr<AbstractRenderer> currentRenderer = rendererManager->getCurrentRendererPointer();
	int64_t VSITotalCPUTime = 0;
	int64_t VSITotalGPUTime = 0;
	std::string shaderName = "";

	rendererManager->setCurrentRenderer("VSInstancedRendering");
	currentRenderer = rendererManager->getCurrentRendererPointer();

	for (size_t shader = 0; shader < 3; ++shader)
	{
		if (shader == 0)
		{ 
			shaderName = "HighComplexity"; 
		}
		else if (shader == 1)
		{
			shaderName = "MidComplexity";
		}
		else
		{
			shaderName = "LowComplexity";
		}
		currentRenderer->setCurrentShader(shaderName);
		sceneManager->setCurrentScene("1x1x1 Cubes");
		benchmarkLoop(currentRenderer, VSITotalCPUTime, VSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 0].m_VSIAverageCPUTime = VSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 0].m_VSIAverageGPUTime = VSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 0].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 0].m_objectCount = 1;
	
		sceneManager->setCurrentScene("32x32x1 Cubes");
		benchmarkLoop(currentRenderer, VSITotalCPUTime, VSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 1].m_VSIAverageCPUTime = VSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 1].m_VSIAverageGPUTime = VSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 1].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 1].m_objectCount = 1024;

		sceneManager->setCurrentScene("32x32x8 Cubes");
		benchmarkLoop(currentRenderer, VSITotalCPUTime, VSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 2].m_VSIAverageCPUTime = VSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 2].m_VSIAverageGPUTime = VSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 2].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 2].m_objectCount = 8192;

		sceneManager->setCurrentScene("32x32x16 Cubes");
		benchmarkLoop(currentRenderer, VSITotalCPUTime, VSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 3].m_VSIAverageCPUTime = VSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 3].m_VSIAverageGPUTime = VSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 3].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 3].m_objectCount = 16384;

		sceneManager->setCurrentScene("32x32x32 Cubes");
		benchmarkLoop(currentRenderer, VSITotalCPUTime, VSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 4].m_VSIAverageCPUTime = VSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 4].m_VSIAverageGPUTime = VSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 4].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 4].m_objectCount = 32768;
	}
}

void Application::benchmarkLoopGSI(std::vector<BenchmarkData>& p_benchmarkDataVector)
{
	std::shared_ptr<RendererManager> rendererManager = m_serviceManager->getRendererManager();
	std::shared_ptr<SceneManager> sceneManager = m_serviceManager->getSceneManager();
	std::shared_ptr<AbstractRenderer> currentRenderer = rendererManager->getCurrentRendererPointer();
	int64_t GSITotalCPUTime = 0;
	int64_t GSITotalGPUTime = 0;
	std::string shaderName = "";

	rendererManager->setCurrentRenderer("GSInstancedRendering");
	currentRenderer = rendererManager->getCurrentRendererPointer();

	for (size_t shader = 0; shader < 3; ++shader)
	{
		if (shader == 0)
		{
			shaderName = "HighComplexity";
		}
		else if (shader == 1)
		{
			shaderName = "MidComplexity";
		}
		else
		{
			shaderName = "LowComplexity";
		}
		currentRenderer->setCurrentShader(shaderName);
		sceneManager->setCurrentScene("1x1x1 Cubes");
		benchmarkLoop(currentRenderer, GSITotalCPUTime, GSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 0].m_GSIAverageCPUTime = GSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 0].m_GSIAverageGPUTime = GSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 0].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 0].m_objectCount = 1;

		sceneManager->setCurrentScene("32x32x1 Cubes");
		benchmarkLoop(currentRenderer, GSITotalCPUTime, GSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 1].m_GSIAverageCPUTime = GSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 1].m_GSIAverageGPUTime = GSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 1].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 1].m_objectCount = 1024;

		sceneManager->setCurrentScene("32x32x8 Cubes");
		benchmarkLoop(currentRenderer, GSITotalCPUTime, GSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 2].m_GSIAverageCPUTime = GSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 2].m_GSIAverageGPUTime = GSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 2].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 2].m_objectCount = 8192;

		sceneManager->setCurrentScene("32x32x16 Cubes");
		benchmarkLoop(currentRenderer, GSITotalCPUTime, GSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 3].m_GSIAverageCPUTime = GSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 3].m_GSIAverageGPUTime = GSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 3].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 3].m_objectCount = 16384;

		sceneManager->setCurrentScene("32x32x32 Cubes");
		benchmarkLoop(currentRenderer, GSITotalCPUTime, GSITotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 4].m_GSIAverageCPUTime = GSITotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 4].m_GSIAverageGPUTime = GSITotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 4].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 4].m_objectCount = 32768;
	}
}

void Application::benchmarkLoopGSD(std::vector<BenchmarkData>& p_benchmarkDataVector)
{
	std::shared_ptr<RendererManager> rendererManager = m_serviceManager->getRendererManager();
	std::shared_ptr<SceneManager> sceneManager = m_serviceManager->getSceneManager();
	std::shared_ptr<AbstractRenderer> currentRenderer = rendererManager->getCurrentRendererPointer();
	int64_t GSDTotalCPUTime = 0;
	int64_t GSDTotalGPUTime = 0;
	std::string shaderName = "";

	rendererManager->setCurrentRenderer("GSDuplicatedGeometry");
	currentRenderer = rendererManager->getCurrentRendererPointer();

	for (size_t shader = 0; shader < 3; ++shader)
	{
		if (shader == 0)
		{
			shaderName = "HighComplexity";
		}
		else if (shader == 1)
		{
			shaderName = "MidComplexity";
		}
		else
		{
			shaderName = "LowComplexity";
		}
		currentRenderer->setCurrentShader(shaderName);
		sceneManager->setCurrentScene("1x1x1 Cubes");
		benchmarkLoop(currentRenderer, GSDTotalCPUTime, GSDTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 0].m_GSDAverageCPUTime = GSDTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 0].m_GSDAverageGPUTime = GSDTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 0].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 0].m_objectCount = 1;
	
		sceneManager->setCurrentScene("32x32x1 Cubes");
		benchmarkLoop(currentRenderer, GSDTotalCPUTime, GSDTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 1].m_GSDAverageCPUTime = GSDTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 1].m_GSDAverageGPUTime = GSDTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 1].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 1].m_objectCount = 1024;

		sceneManager->setCurrentScene("32x32x8 Cubes");
		benchmarkLoop(currentRenderer, GSDTotalCPUTime, GSDTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 2].m_GSDAverageCPUTime = GSDTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 2].m_GSDAverageGPUTime = GSDTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 2].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 2].m_objectCount = 8192;

		sceneManager->setCurrentScene("32x32x16 Cubes");
		benchmarkLoop(currentRenderer, GSDTotalCPUTime, GSDTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 3].m_GSDAverageCPUTime = GSDTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 3].m_GSDAverageGPUTime = GSDTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 3].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 3].m_objectCount = 16384;

		sceneManager->setCurrentScene("32x32x32 Cubes");
		benchmarkLoop(currentRenderer, GSDTotalCPUTime, GSDTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 4].m_GSDAverageCPUTime = GSDTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 4].m_GSDAverageGPUTime = GSDTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 4].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 4].m_objectCount = 32768;
	}
}

void Application::benchmarkLoopDDC(std::vector<BenchmarkData>& p_benchmarkDataVector)
{
	std::shared_ptr<RendererManager> rendererManager = m_serviceManager->getRendererManager();
	std::shared_ptr<SceneManager> sceneManager = m_serviceManager->getSceneManager();
	std::shared_ptr<AbstractRenderer> currentRenderer = rendererManager->getCurrentRendererPointer();
	int64_t DDCTotalCPUTime = 0;
	int64_t DDCTotalGPUTime = 0;
	std::string shaderName = "";

	rendererManager->setCurrentRenderer("DuplicateCalls");
	currentRenderer = rendererManager->getCurrentRendererPointer();

	for (size_t shader = 0; shader < 3; ++shader)
	{
		if (shader == 0)
		{
			shaderName = "HighComplexity";
		}
		else if (shader == 1)
		{
			shaderName = "MidComplexity";
		}
		else
		{
			shaderName = "LowComplexity";
		}
		currentRenderer->setCurrentShader(shaderName);
		sceneManager->setCurrentScene("1x1x1 Cubes");
		benchmarkLoop(currentRenderer, DDCTotalCPUTime, DDCTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 0].m_DDCAverageCPUTime = DDCTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 0].m_DDCAverageGPUTime = DDCTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 0].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 0].m_objectCount = 1;

		sceneManager->setCurrentScene("32x32x1 Cubes");
		benchmarkLoop(currentRenderer, DDCTotalCPUTime, DDCTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 1].m_DDCAverageCPUTime = DDCTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 1].m_DDCAverageGPUTime = DDCTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 1].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 1].m_objectCount = 1024;

		sceneManager->setCurrentScene("32x32x8 Cubes");
		benchmarkLoop(currentRenderer, DDCTotalCPUTime, DDCTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 2].m_DDCAverageCPUTime = DDCTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 2].m_DDCAverageGPUTime = DDCTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 2].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 2].m_objectCount = 8192;

		sceneManager->setCurrentScene("32x32x16 Cubes");
		benchmarkLoop(currentRenderer, DDCTotalCPUTime, DDCTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 3].m_DDCAverageCPUTime = DDCTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 3].m_DDCAverageGPUTime = DDCTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 3].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 3].m_objectCount = 16384;

		sceneManager->setCurrentScene("32x32x32 Cubes");
		benchmarkLoop(currentRenderer, DDCTotalCPUTime, DDCTotalGPUTime);
		p_benchmarkDataVector[shader * 5 + 4].m_DDCAverageCPUTime = DDCTotalCPUTime;
		p_benchmarkDataVector[shader * 5 + 4].m_DDCAverageGPUTime = DDCTotalGPUTime;
		p_benchmarkDataVector[shader * 5 + 4].m_shaderName = shaderName;
		p_benchmarkDataVector[shader * 5 + 4].m_objectCount = 32768;
	}
}

void Application::benchmarkLoop(std::shared_ptr<AbstractRenderer> p_currentRenderer, int64_t& p_CPUAverageTime, int64_t& p_GPUAverageTime)
{
	p_CPUAverageTime = 0;
	p_GPUAverageTime = 0;

	p_currentRenderer->update();
	p_currentRenderer->render();
	p_currentRenderer->update();
	p_currentRenderer->render();

	for (size_t currentFrame = 0; currentFrame < 2000; ++currentFrame)
	{
		p_currentRenderer->update();
		p_currentRenderer->render();
		p_CPUAverageTime += p_currentRenderer->m_updateCallCPUDurationInNanoseconds;
		p_GPUAverageTime += p_currentRenderer->m_renderCallGPUDurationInNanoSeconds;
	}

	p_CPUAverageTime /= 2000;
	p_GPUAverageTime /= 2000;
}

void Application::submitBenchmarkData(const BenchmarkData& p_submissionData)
{
	//std::string reqBody;
	char* reqBodyTemp = new char[2097152]; //2KB buffer
#ifdef _WIN64
	_snprintf(reqBodyTemp, 2097152, "OS=%s&UUID=%s&GSDCPU=%ld&GSDGPU=%ld&VSICPU=%ld&VSIGPU=%ld&GSICPU=%ld&GSIGPU=%ld&DDCCPU=%ld&DDCGPU=%ld&objectCount=%ld&gpuName=%s&shader=%s",
		p_submissionData.m_OS.c_str(), p_submissionData.m_UUID.c_str(),
		p_submissionData.m_GSDAverageCPUTime, p_submissionData.m_GSDAverageGPUTime,
		p_submissionData.m_VSIAverageCPUTime, p_submissionData.m_VSIAverageGPUTime,
		p_submissionData.m_GSIAverageCPUTime, p_submissionData.m_GSIAverageGPUTime,
		p_submissionData.m_DDCAverageCPUTime, p_submissionData.m_DDCAverageGPUTime,
		p_submissionData.m_objectCount, p_submissionData.m_gpuName.c_str(), p_submissionData.m_shaderName.c_str());
#else
	snprintf(reqBodyTemp, 2097152, "OS=%s&UUID=%s&GSDCPU=%ld&GSDGPU=%ld&VSICPU=%ld&VSIGPU=%ld&GSICPU=%ld&GSIGPU=%ld&DDCCPU=%ld&DDCGPU=%ld&objectCount=%ld&gpuName=%s&shader=%s",
		p_submissionData.m_OS.c_str(), p_submissionData.m_UUID.c_str(),
		p_submissionData.m_GSDAverageCPUTime, p_submissionData.m_GSDAverageGPUTime,
		p_submissionData.m_VSIAverageCPUTime, p_submissionData.m_VSIAverageGPUTime,
		p_submissionData.m_GSIAverageCPUTime, p_submissionData.m_GSIAverageGPUTime,
		p_submissionData.m_DDCAverageCPUTime, p_submissionData.m_DDCAverageGPUTime,
		p_submissionData.m_objectCount, p_submissionData.m_gpuName.c_str(), p_submissionData.m_shaderName.c_str());
#endif

	std::streamsize reqBodyLength = strlen(reqBodyTemp);
	// prepare session
	Poco::URI pocoURI = Poco::URI("http://stereorenderingbenchmark.appspot.com:80/benchmark");
	Poco::Net::HTTPClientSession session(pocoURI.getHost(), pocoURI.getPort());

	// prepare path
	std::string path(pocoURI.getPathAndQuery());

	// prepare request
	Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_POST, path, Poco::Net::HTTPMessage::HTTP_1_1);
	req.setContentLength(reqBodyLength);

	// Send request
	std::ostream& reqostsream = session.sendRequest(req);
	reqostsream << reqBodyTemp;

	// get response
	Poco::Net::HTTPResponse res;

	// print response
	std::istream& is = session.receiveResponse(res);
	std::cout << is.rdbuf() << std::endl;
	delete[] reqBodyTemp;
}