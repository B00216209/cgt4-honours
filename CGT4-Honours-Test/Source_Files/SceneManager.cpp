#include "SceneManager.h"

SceneManager::SceneManager(std::shared_ptr<ServiceManager> p_serviceManager) : m_serviceManager(p_serviceManager), m_currentSceneIndex(0)
{
}

SceneManager::~SceneManager()
{
}

int SceneManager::setCurrentScene(const std::string& p_sceneName)
{
	for (size_t i = 0; i < m_sceneNames.size(); ++i)
	{
		if (m_sceneNames[i] == p_sceneName)
		{
			size_t previousIndex = i;
			m_serviceManager->getRendererManager()->getCurrentRendererPointer()->cleanUpScene();
			m_currentSceneIndex = i;
			m_currentScenePointer = m_scenePointers[i];
			m_serviceManager->getRendererManager()->getCurrentRendererPointer()->initScene(m_currentScenePointer);
			std::cout << "Changed Scene from " << m_sceneNames[previousIndex] << " to " << m_sceneNames[m_currentSceneIndex] << std::endl;
			return 0;
		}
	}
	return 1;
}

void SceneManager::goToNextScene()
{
	if (m_currentSceneIndex < m_scenePointers.size() - 1)
	{
		m_serviceManager->getRendererManager()->getCurrentRendererPointer()->cleanUpScene();
		m_currentSceneIndex++;
		m_currentScenePointer = m_scenePointers[m_currentSceneIndex];
		m_serviceManager->getRendererManager()->getCurrentRendererPointer()->initScene(m_currentScenePointer);
		std::cout << "Changed Scene from " << m_sceneNames[m_currentSceneIndex - 1] << " to " << m_sceneNames[m_currentSceneIndex] << std::endl;
	}
	else
	{
		m_serviceManager->getRendererManager()->getCurrentRendererPointer()->cleanUpScene();
		m_currentSceneIndex = 0;
		m_currentScenePointer = m_scenePointers[m_currentSceneIndex];
		m_serviceManager->getRendererManager()->getCurrentRendererPointer()->initScene(m_currentScenePointer);
		std::cout << "Changed Scene from " << m_sceneNames[m_scenePointers.size() - 1] << " to " << m_sceneNames[m_currentSceneIndex] << std::endl;
	}
}

void SceneManager::goToPreviousScene()
{
	if (m_currentSceneIndex > 0)
	{
		m_serviceManager->getRendererManager()->getCurrentRendererPointer()->cleanUpScene();
		m_currentSceneIndex--;
		m_currentScenePointer = m_scenePointers[m_currentSceneIndex];
		m_serviceManager->getRendererManager()->getCurrentRendererPointer()->initScene(m_currentScenePointer);
		std::cout << "Changed Scene from " << m_sceneNames[m_currentSceneIndex + 1] << " to " << m_sceneNames[m_currentSceneIndex] << std::endl;

	}
	else
	{
		m_serviceManager->getRendererManager()->getCurrentRendererPointer()->cleanUpScene();
		m_currentSceneIndex = m_scenePointers.size() - 1;
		m_currentScenePointer = m_scenePointers[m_currentSceneIndex];
		m_serviceManager->getRendererManager()->getCurrentRendererPointer()->initScene(m_currentScenePointer);
		std::cout << "Changed Scene from " << m_sceneNames[0] << " to " << m_sceneNames[m_currentSceneIndex] << std::endl;
	}
}

int SceneManager::init()
{
	std::shared_ptr<DynamicCubeScene> rDynamicCubeScene1x1x1 = std::make_shared<DynamicCubeScene>(glm::uvec3(1, 1, 1));
	if (rDynamicCubeScene1x1x1->init() != 0)
	{
		std::cout << "3DCubeScene::Init() Error! " << std::endl;
		return -1;
	}
	m_scenePointers.push_back(rDynamicCubeScene1x1x1);
	m_sceneNames.push_back("1x1x1 Cubes");

	std::shared_ptr<DynamicCubeScene> rDynamicCubeScene32x32x1 = std::make_shared<DynamicCubeScene>(glm::uvec3(32, 32, 1));
	if (rDynamicCubeScene32x32x1->init() != 0)
	{
		std::cout << "3DCubeScene::Init() Error! " << std::endl;
		return -1;
	}
	m_scenePointers.push_back(rDynamicCubeScene32x32x1);
	m_sceneNames.push_back("32x32x1 Cubes");

	std::shared_ptr<DynamicCubeScene> rDynamicCubeScene32x32x8 = std::make_shared<DynamicCubeScene>(glm::uvec3(32, 32, 8));
	if (rDynamicCubeScene32x32x8->init() != 0)
	{
		std::cout << "3DCubeScene::Init() Error! " << std::endl;
		return -1;
	}
	m_scenePointers.push_back(rDynamicCubeScene32x32x8);
	m_sceneNames.push_back("32x32x8 Cubes");

	std::shared_ptr<DynamicCubeScene> rDynamicCubeScene32x32x16 = std::make_shared<DynamicCubeScene>(glm::uvec3(32, 32, 16));
	if (rDynamicCubeScene32x32x16->init() != 0)
	{
		std::cout << "3DCubeScene::Init() Error! " << std::endl;
		return -1;
	}
	m_scenePointers.push_back(rDynamicCubeScene32x32x16);
	m_sceneNames.push_back("32x32x16 Cubes");

	std::shared_ptr<DynamicCubeScene> rDynamicCubeScene32x32x32 = std::make_shared<DynamicCubeScene>(glm::uvec3(32, 32, 32));
	if (rDynamicCubeScene32x32x32->init() != 0)
	{
		std::cout << "3DCubeScene::Init() Error! " << std::endl;
		return -1;
	}
	m_scenePointers.push_back(rDynamicCubeScene32x32x32);
	m_sceneNames.push_back("32x32x32 Cubes");

	m_currentSceneIndex = 0;
	m_currentScenePointer = m_scenePointers[0];

	return 0;
}

const std::shared_ptr<AbstractScene> SceneManager::getCurrentScenePointer() const
{
	return m_currentScenePointer;
}
