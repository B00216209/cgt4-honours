#include "ShaderManager.h"

ShaderManager::ShaderManager(std::shared_ptr<ServiceManager> p_serviceManager) : m_ServiceManager(p_serviceManager)
{
}


ShaderManager::~ShaderManager(void)
{
}

GLuint ShaderManager::loadGLShader(const std::string& p_vertName, const std::string& p_fragName)
{
	GLuint v = glCreateShader(GL_VERTEX_SHADER);
	GLuint f = glCreateShader(GL_FRAGMENT_SHADER);

	GLint vlen = 0;
	GLint flen = 0;


	bool programCheck = glIsShader(v);
	if (!programCheck)
	{
		std::cout << "PreShaderSource Vertex Shader is not a valid shader: " << v << std::endl;
	}
	programCheck = glIsShader(f);
	if (!programCheck)
	{
		std::cout << "PreShaderSource Fragment Shader is not a valid shader: " << f << std::endl;
	}


	const char* vs = loadFile(p_vertName, vlen);
	const char* fs = loadFile(p_fragName, flen);

	glShaderSource(v, 1, &vs, &vlen);
	glShaderSource(f, 1, &fs, &flen);

	programCheck = glIsShader(v);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostShaderSource Vertex Shader is not a valid shader: " << v << std::endl;
	}
	programCheck = glIsShader(f);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostShaderSource Fragment Shader is not a valid shader: " << f << std::endl;
	}

	GLint compiled = 0;

	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		std::cout << "Vertex shader not compiled: " << p_vertName << std::endl;
		printShaderError(v);
		return -1;
	}
	else
	{
		std::cout << "Vertex shader compiled: " << p_vertName << std::endl;
		printShaderError(v);
	}

	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		std::cout << "Fragment shader not compiled: " << p_fragName << std::endl;
		printShaderError(f);
		return -1;
	}
	else
	{
		std::cout << "Fragment shader compiled: " << p_fragName << std::endl;
		printShaderError(f);
	}

	programCheck = glIsShader(v);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostCompile Vertex Shader is not a valid shader: " << v << std::endl;
		return -1;
	}

	programCheck = glIsShader(f);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostCompile Fragment Shader is not a valid shader: " << f << std::endl;
		return -1;
	}

	GLuint p = glCreateProgram();
	programCheck = glIsProgram(p);
	if (programCheck == GL_FALSE)
	{
		std::cout << "Program is not a valid program: " << p << std::endl;
		return -1;
	}

	glAttachShader(p, v);
	glAttachShader(p, f);

	glLinkProgram(p);
	GLint isLinked = GL_FALSE;
	glGetProgramiv(p, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		std::cout << "Program failed to link: " << p << std::endl;
		printShaderError(p);
		return -1;
	}
	else
	{
		std::cout << "Program linked sucessfully: " << p << std::endl;
		printShaderError(p);
	}

	glDetachShader(p, v);
	glDetachShader(p, f);

	delete[] vs;
	delete[] fs;

	return p;
}


GLuint ShaderManager::loadGLShader(const std::string& p_vertName, const std::string& p_geoName, const std::string& p_fragName)
{
	GLuint v = glCreateShader(GL_VERTEX_SHADER);
	GLuint g = glCreateShader(GL_GEOMETRY_SHADER);
	GLuint f = glCreateShader(GL_FRAGMENT_SHADER);

	GLint vlen = 0;
	GLint glen = 0;
	GLint flen = 0;


	bool programCheck = glIsShader(v);
	if (!programCheck)
	{
		std::cout << "PreShaderSource Vertex Shader is not a valid shader: " << v << std::endl;
		return -1;
	}
	programCheck = glIsShader(g);
	if (!programCheck)
	{
		std::cout << "PreShaderSource Geometry Shader is not a valid shader: " << g << std::endl;
		return -1;
	}
	programCheck = glIsShader(f);
	if (!programCheck)
	{
		std::cout << "PreShaderSource Fragment Shader is not a valid shader: " << f << std::endl;
		return -1;
	}

	const char* vs = loadFile(p_vertName, vlen);
	const char* gs = loadFile(p_geoName, glen);
	const char* fs = loadFile(p_fragName, flen);

	glShaderSource(v, 1, &vs, &vlen);
	glShaderSource(g, 1, &gs, &glen);
	glShaderSource(f, 1, &fs, &flen);

	programCheck = glIsShader(v);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostShaderSource Vertex Shader is not a valid shader: " << v << std::endl;
		return -1;
	}
	programCheck = glIsShader(g);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostShaderSource Geometry Shader is not a valid shader: " << f << std::endl;
		return -1;
	}
	programCheck = glIsShader(f);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostShaderSource Fragment Shader is not a valid shader: " << f << std::endl;
		return -1;
	}

	GLint compiled = 0;

	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		std::cout << "Vertex shader not compiled: " << p_vertName << std::endl;
		printShaderError(v);
		return -1;
	}
	else
	{
		std::cout << "Vertex shader compiled: " << p_vertName << std::endl;
		printShaderError(v);
	}

	glCompileShader(g);
	glGetShaderiv(g, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		std::cout << "Geometry shader not compiled: " << p_geoName << std::endl;
		printShaderError(g);
		return -1;
	}
	else
	{
		std::cout << "Geometry shader compiled: " << p_geoName << std::endl;
		printShaderError(g);
	}

	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		std::cout << "Fragment shader not compiled: " << p_fragName << std::endl;
		printShaderError(f);
		return -1;
	}
	else
	{
		std::cout << "Fragment shader compiled: " << p_fragName << std::endl;
		printShaderError(f);
	}

	programCheck = glIsShader(v);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostCompile Vertex Shader is not a valid shader: " << v << std::endl;
		return -1;
	}
	programCheck = glIsShader(g);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostCompile Geometry Shader is not a valid shader: " << g << std::endl;
		return -1;
	}
	programCheck = glIsShader(f);
	if (programCheck == GL_FALSE)
	{
		std::cout << "PostCompile Fragment Shader is not a valid shader: " << f << std::endl;
		return -1;
	}

	GLuint p = glCreateProgram();
	programCheck = glIsProgram(p);
	if (programCheck == GL_FALSE)
	{
		std::cout << "Program is not a valid program: " << p << std::endl;
		return -1;
	}

	glAttachShader(p, v);
	glAttachShader(p, g);
	glAttachShader(p, f);

	glLinkProgram(p);
	GLint isLinked = GL_FALSE;
	glGetProgramiv(p, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		std::cout << "Program failed to link: " << p << std::endl;
		printShaderError(p);
		return -1;
	}
	else
	{
		std::cout << "Program linked sucessfully: " << p << std::endl;
		printShaderError(p);
	}

	glDetachShader(p, v);
	glDetachShader(p, g);
	glDetachShader(p, f);

	delete[] vs;
	delete[] gs;
	delete[] fs;

	return p;
}

const char* ShaderManager::loadFile(const std::string& p_fname, GLint& p_size)
{

	std::ifstream file(p_fname.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
	if (file.is_open())
	{
		char* fileContents;
		file.seekg(0, file.end);
		size_t charCount = file.tellg();
		fileContents = new char[charCount];
		p_size = static_cast<GLint>(charCount);
		file.seekg(0, file.beg);
		file.read(fileContents, charCount);
		file.close();
		std::cout << "file " << p_fname.c_str() << " loaded" << std::endl;
		return fileContents;
	}
	else
	{
		std::cout << "Unable to open file " << p_fname.c_str() << std::endl;
		return "ERROR: Unable to open file.";
	}
}


// from rt3d.h by Daniel Livingstone
void ShaderManager::printShaderError(const GLuint shader)
{
	GLint logLength = 0;

	if (!glIsShader(shader))
		glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &logLength);
	else
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

	if (logLength > 0)
	{
		GLchar *logMessage = new GLchar[logLength];
		if (!glIsShader(shader))
		{
			glGetProgramInfoLog(shader, logLength, &logLength, logMessage);
		}
		else
		{
			glGetShaderInfoLog(shader, logLength, &logLength, logMessage);
		}
		std::cout << "Shader Info Log: " << logMessage << std::endl;
		delete[] logMessage;
	}
}
