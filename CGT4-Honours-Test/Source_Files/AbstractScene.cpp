#include "AbstractScene.h"

AbstractScene::~AbstractScene()
{
}

const std::vector<const aiScene*>& AbstractScene::getSceneVector() const
{
	return m_aiSceneVector;
}

const std::vector<glm::vec4>& AbstractScene::getPositionVector() const
{
	return m_positionVector;
}

const std::vector<glm::fquat>& AbstractScene::getOrientationVector() const
{
	return m_orientationVector;
}

const std::vector<glm::mat4>& AbstractScene::getModelMatrixVector() const
{
	return m_modelMatrixVector;
}

const std::vector<GLuint>& AbstractScene::getperMeshInstanceCountVector() const
{
	return m_perMeshInstanceCount;
}

const std::vector<std::string>& AbstractScene::getTextureFilenameVector() const
{
	return m_textureFilenameVector;
}

const std::vector<GLuint>& AbstractScene::getRenderableObjectTextureIDVector() const
{
	return m_renderableObjectTextureIDVector;
}

GLuint AbstractScene::getRenderableObjectCount() const
{
	return m_renderableObjectCount;
}

GLuint AbstractScene::getVertexCount() const
{
	return m_vertexCount;
}

GLuint AbstractScene::getIndexCount() const
{
	return m_indexCount;
}

GLuint AbstractScene::getTextureCount() const
{
	return m_textureCount;
}

int AbstractScene::importFiletoAiScene(const std::string& p_filePath)
{
	const aiScene* scene = aiImportFile(p_filePath.c_str(), aiProcess_RemoveRedundantMaterials);

	if (scene == nullptr)
	{
		std::cout << aiGetErrorString() << std::endl;
		return 1;
	}

	m_aiSceneVector.push_back(scene);
	m_vertexCount += scene->mMeshes[0]->mNumVertices;
	m_indexCount += scene->mMeshes[0]->mNumFaces * 3;
	return 0;
}
