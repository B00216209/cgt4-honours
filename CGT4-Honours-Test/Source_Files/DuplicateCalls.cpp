#include "DuplicateCalls.h"

DuplicateCalls::DuplicateCalls(std::shared_ptr<ServiceManager> p_serviceManager) : AbstractRenderer(p_serviceManager)
{
}

DuplicateCalls::~DuplicateCalls()
{
	if (m_vertexBufferObjectVector.size() != 0) cleanUpScene();
}

int DuplicateCalls::initScene(const std::shared_ptr<AbstractScene>& p_currentScene)
{
	if (m_vertexBufferObjectVector.size() != 0) cleanUpScene();
	sceneSetup(1, p_currentScene);
	return 0;
}

int DuplicateCalls::cleanUpScene()
{
	sceneCleanUp();
	return 0;
}


int DuplicateCalls::init()
{
	std::cout << "DuplicateCalls::init() started." << std::endl;

	if (initShaders("shaders/default.vert", "", "shaders/unlit_Texture.frag",
		"shaders/default.vert", "", "shaders/lit_LambertianDiffuse.frag",
		"shaders/default.vert", "", "shaders/lit_PBR.frag") == -1)
	{
		return -1;
	}

	initMultipleViewports();
	initViewandProjectionMatrices();

	return 0;
}

void DuplicateCalls::refresh()
{
	glClearColor(0.2f, 0.7f, 0.7f, 1.0f);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDisable(GL_BLEND);
	glDisable(GL_SCISSOR_TEST);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_FALSE);
	glDepthMask(GL_TRUE);
	setCurrentShader("HighComplexity");
}

void DuplicateCalls::render()
{
	renderMultipleViewports();
}