#include "ServiceManager.h"

ServiceManager::ServiceManager()
{
}


ServiceManager::~ServiceManager()
{
}

std::shared_ptr<WindowManager> ServiceManager::getWindowManager() const
{
	return m_windowManager;
}

void ServiceManager::setWindowManager(std::shared_ptr<WindowManager> p_windowManager)
{
	m_windowManager = p_windowManager;
}

std::shared_ptr<RendererManager> ServiceManager::getRendererManager() const
{
	return m_rendererManager;
}

void ServiceManager::setRendererManager(std::shared_ptr<RendererManager> p_renderManager)
{
	m_rendererManager = p_renderManager;
}

std::shared_ptr<SceneManager> ServiceManager::getSceneManager() const
{
	return m_sceneManager;
}

void ServiceManager::setSceneManager(std::shared_ptr<SceneManager> p_sceneManager)
{
	m_sceneManager = p_sceneManager;
}

std::shared_ptr<ShaderManager> ServiceManager::getShaderManager() const
{
	return m_shaderManager;
}

void ServiceManager::setShaderManager(std::shared_ptr<ShaderManager> p_shaderManager)
{
	m_shaderManager = p_shaderManager;
}

