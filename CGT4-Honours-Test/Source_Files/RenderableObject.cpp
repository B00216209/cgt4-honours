#include "RenderableObject.h"

RenderableObject::RenderableObject(GLuint p_indexCount, GLuint p_indexOffset, GLuint p_instanceCount, GLuint p_textureOffset, glm::vec4 p_position, glm::fquat p_orientation)
	: m_indexCount(p_indexCount),
	m_indexOffset(p_indexOffset),
	m_instanceCount(p_instanceCount),
	m_textureOffset(p_textureOffset),
	m_position(p_position),
	m_orientation(p_orientation),
	m_modelMatrix(glm::mat4(1.0f))
{
	updateModelMatrix();
}

RenderableObject::~RenderableObject()
{
}

GLuint RenderableObject::getIndexCount() const
{
	return m_indexCount;
}

GLuint RenderableObject::getIndexOffset() const
{
	return m_indexOffset;
}

GLuint RenderableObject::getInstanceCount() const
{
	return m_instanceCount;
}

GLuint RenderableObject::getTextureOffset() const
{
	return m_textureOffset;
}

glm::vec4 RenderableObject::getPosition() const
{
	return m_position;
}

glm::fquat RenderableObject::getOrientation() const
{
	return m_orientation;
}

glm::mat4 RenderableObject::getModelMatrix() const
{
	return m_modelMatrix;
}

void RenderableObject::setPosition(glm::vec4 p_position)
{
	m_position = p_position;
	updateModelMatrix();
}
void RenderableObject::setOrientation(glm::fquat p_orientation)
{
	m_orientation = p_orientation;
	updateModelMatrix();
}

void RenderableObject::updateModelMatrix()
{
	m_modelMatrix = glm::mat4(1.0f);
	m_modelMatrix = glm::translate(m_modelMatrix, glm::vec3(m_position));
	m_modelMatrix = m_modelMatrix * glm::mat4_cast(m_orientation);
}
