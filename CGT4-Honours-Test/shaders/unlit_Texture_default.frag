// passThrough.frag
#version 440

layout(binding = 0) uniform sampler2DArray diffuseTextureArray;

smooth in vec2 texCoord;
flat in uint texLayer;

layout(location=0, index = 0) out vec4 FS_FB_diffuse;

void main() 
{
	FS_FB_diffuse = vec4(texture(diffuseTextureArray, vec3(texCoord, texLayer)).rgb,1.0);
}