// PBR shader, adapted from [Karis13] UE4 course notes.
#version 440

layout(binding = 0) uniform sampler2DArray diffuseTextureArray;

smooth in vec3 viewSpacePosition;
smooth in vec3 viewSpaceNormal;
smooth in vec2 texCoord;
flat in uint texLayer;

layout(location=0, index = 0) out vec4 FS_FB_SRGB;

const float PI = 3.141592653589793238462643383 ;

vec3 F_Schlick(vec3 f0, float f90, float u)
{
	return f0 + (vec3(f90) - f0) * pow(1.0 - u, 5.0);
}

float G1V(float NdotV, float k)
{
	return 1.0f/(NdotV * (1.0-k) + k);
}

float V_SmithGXX(float NdotL, float NdotV, float roughness)
{
	float halfRoughness2 = roughness * roughness * 0.5f;
		
	return G1V(NdotL, halfRoughness2) * G1V(NdotV, halfRoughness2);
}

float D_GGX(float NdotH, float roughness)
{
	float m2 = roughness*roughness;
	float f = (NdotH * m2 - NdotH) * NdotH + 1;
	return m2 / (PI * f * f);
}

float DisneyDiffuseRenormalized(float NdotV, float NdotL, float LdotH, float linearRoughness)
{
	float energyBias = mix(0.0, 0.5, linearRoughness);
	float energyFactor = mix(1.0, 1.0/1.51, linearRoughness);
	float fd90 = energyBias + 2.0 * LdotH * LdotH * linearRoughness;
	vec3 f0 = vec3(1.0);
	float lightScatter = F_Schlick(f0, fd90, NdotL).r;
	float viewScatter = F_Schlick(f0, fd90, NdotV).r;
	
	return lightScatter * lightScatter * energyFactor;
}

float lightFalloff(float distance, float Intensity)
{
	return Intensity * 1 / pow(distance,2);
}

void main() 
{
	const vec4 tex1 = texture(diffuseTextureArray, vec3(texCoord, texLayer));
	const vec4 tex2 = vec4(viewSpaceNormal,0.95);
	
	const vec3 albedo = tex1.rgb;
	const vec3 f0 = vec3(0.10,0.10,0.10);
		
	const vec3 N = normalize(tex2.rgb);
	const float smoothness = 0.25;
	const float roughness = 1 - smoothness;;
	const float linearRoughness = pow(roughness, 4);
		
	vec3 L = vec3(0.0,0.0,-50.0) - viewSpacePosition;
	const float lightDistance = length(L);
	L = normalize(L);
	const vec3 V = normalize(-viewSpacePosition);
	const float NdotV = clamp(dot(N, V), 0.0, 1.0);
	const vec3 H = normalize(V + L);
	const float NdotL = clamp(dot(N, L), 0.0, 1.0);
	const float NdotH = clamp(dot(N, H), 0.0, 1.0);
	const float LdotH = clamp(dot(L, H), 0.0, 1.0);
	
	const float lightIntensity = 2400.0;
	const vec3 lightColour = vec3(1.0,1.0,1.0);
	
	const vec3 lightTotal = lightFalloff(lightDistance, lightIntensity) * lightColour;
		
	// Specular BRDF
	const vec3 F = F_Schlick(f0, 1.0, LdotH);
	const float Vis = V_SmithGXX(NdotV, NdotL, roughness);
	const float D = D_GGX(NdotH, roughness);
	const vec3 Fr = D * F * Vis * NdotL;
	
	// Diffuse BRDF
	const float Fd = DisneyDiffuseRenormalized(NdotV, NdotL, LdotH, linearRoughness);
	
	const vec3 diffuseOutput =  Fd * albedo/PI * lightTotal;
	const vec3 specularOutput = Fr * lightTotal;
	
	FS_FB_SRGB = vec4(pow(diffuseOutput+specularOutput,vec3(2.2)),1.0);
}