// GSDuplicatedGeometry.geo, a default vertex shader for use with GS duplicated geometry multi-view rendering
#version 440

layout(triangles) in;
layout(triangle_strip, max_vertices=6) out;

layout (std140, binding = 0) buffer modelMatrixBuffer
{
    mat4 modelMatrixArray[];
};

uniform mat4 projectionMatrices[2];
uniform mat4 viewMatrices[2];

smooth in vec3 objectSpaceNormalArray[];
smooth in vec2 texCoordArray[];
flat in uvec2 drawParametersArray[];

smooth out vec3 viewSpacePosition;
smooth out vec3 viewSpaceNormal;
smooth out vec2 texCoord;
flat out uint texLayer;

void main() 
{
	vec4 position;
	mat4 modelViewMatrix;
	mat3 normalMatrix;
	for(int viewIndex = 0; viewIndex < 2; ++viewIndex)
	{
		gl_ViewportIndex = viewIndex;
		modelViewMatrix = viewMatrices[viewIndex] * modelMatrixArray[drawParametersArray[0].x];
		normalMatrix = transpose(inverse(mat3(modelMatrixArray[drawParametersArray[0].x] * viewMatrices[viewIndex])));
		for(uint vertexIndex = 0; vertexIndex < 3; ++vertexIndex)
		{
			position = modelViewMatrix * gl_in[vertexIndex].gl_Position;
			viewSpacePosition = position.xyz;
			viewSpaceNormal = normalMatrix * objectSpaceNormalArray[vertexIndex];
			texCoord = texCoordArray[vertexIndex];
			texLayer = drawParametersArray[vertexIndex].y;
			gl_Position = projectionMatrices[viewIndex] * position;
			EmitVertex();
		}
		EndPrimitive();
	}
}