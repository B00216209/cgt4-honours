// GSInstancedRendering.geo, a default geometry shader for use with GS indexed viewport multi-view rendering
#version 440

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

layout (std140, binding = 0) buffer modelMatrixBuffer
{
    mat4 modelMatrixArray[];
};

uniform mat4 projectionMatrices[2];
uniform mat4 viewMatrices[2];

smooth in vec3 objectSpaceNormalArray[];
smooth in vec2 texCoordArray[];
flat in uvec2 drawParametersArray[];
flat in int instanceID[];

smooth out vec3 viewSpacePosition;
smooth out vec3 viewSpaceNormal;
smooth out vec2 texCoord;
flat out uint texLayer;

void main() 
{
	gl_ViewportIndex = instanceID[0];
	const mat4 modelViewMatrix = viewMatrices[instanceID[0]] * modelMatrixArray[drawParametersArray[0].x];
	const mat3 normalMatrix = transpose(inverse(mat3(modelMatrixArray[drawParametersArray[0].x] * viewMatrices[instanceID[0]])));
	vec4 position;
	for(uint i = 0; i < 3; ++i)
	{
		position = modelViewMatrix * gl_in[i].gl_Position;
		viewSpacePosition = position.xyz;
		viewSpaceNormal = normalMatrix * objectSpaceNormalArray[i];
		texCoord = texCoordArray[i];
		texLayer = drawParametersArray[i].y;
		gl_Position = projectionMatrices[instanceID[0]] * position;
		EmitVertex();
	}
	EndPrimitive();
}