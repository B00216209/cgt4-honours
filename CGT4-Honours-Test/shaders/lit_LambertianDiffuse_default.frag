// passThrough.frag
#version 440

layout(binding = 0) uniform sampler2DArray diffuseTextureArray;

smooth in vec3 viewSpacePosition;
smooth in vec3 viewSpaceNormal;
smooth in vec2 texCoord;
flat in uint texLayer;

layout(location=0, index = 0) out vec4 FS_FB_diffuse;

void main() 
{
	const vec3 diffuseTexel = texture(diffuseTextureArray, vec3(texCoord, texLayer)).rgb;
	const vec3 N = normalize(viewSpaceNormal);
	const vec3 L = normalize(viewSpacePosition - vec3(0.0,0.0,0.0));
	const float lambert = clamp(dot(N,-L),0.0,1.0);
		
	FS_FB_diffuse = vec4(diffuseTexel*lambert, 1.0);
}