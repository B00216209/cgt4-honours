// passThrough.vert
#version 440

layout (location = 0) in vec3 VS_position;
layout (location = 1) in vec3 VS_normal;
layout (location = 2) in vec2 VS_texCoord;
layout (location = 3) in uvec2 VS_drawParameters;

layout (std140, binding = 0) buffer modelMatrixBuffer
{
    mat4 modelMatrixArray[];
};

uniform mat4 projectionMatrices[2];
uniform mat4 viewMatrices[2];
uniform int viewportIndex;

smooth out vec3 viewSpacePosition;
smooth out vec3 viewSpaceNormal;
smooth out vec2 texCoord;
flat out uint texLayer;

void main() 
{
	const mat4 modelViewMatrix = viewMatrices[viewportIndex] * modelMatrixArray[VS_drawParameters.x];
	const mat3 normalMatrix = transpose(inverse(mat3(modelMatrixArray[VS_drawParameters.x] * viewMatrices[viewportIndex])));
	vec4 position = modelViewMatrix * vec4(VS_position,1.0);
	viewSpacePosition = position.xyz;
	viewSpaceNormal = normalMatrix * VS_normal;
	texCoord = VS_texCoord;
	texLayer = VS_drawParameters.y;
	gl_Position = projectionMatrices[viewportIndex] * position;
}