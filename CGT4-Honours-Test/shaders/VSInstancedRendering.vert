// InstancedRendering.vert, a default vertex shader for use with VS indexed viewport multi-view rendering
#version 440 core
#extension GL_AMD_vertex_shader_viewport_index : enable
#extension GL_NV_viewport_array2 : enable

layout (location = 0) in vec3 VS_position;
layout (location = 1) in vec3 VS_normal;
layout (location = 2) in vec2 VS_texCoord;
layout (location = 3) in uvec2 VS_drawParameters;

layout (std140, binding = 0) buffer modelMatrixBuffer
{
    mat4 modelMatrixArray[];
};

uniform mat4 projectionMatrices[2];
uniform mat4 viewMatrices[2];

smooth out vec3 viewSpacePosition;
smooth out vec3 viewSpaceNormal;
smooth out vec2 texCoord;
flat out uint texLayer;

void main() 
{
	const mat4 modelViewMatrix = viewMatrices[gl_InstanceID] * modelMatrixArray[VS_drawParameters.x];
	const mat3 normalMatrix = transpose(inverse(mat3(modelMatrixArray[VS_drawParameters.x] * viewMatrices[gl_InstanceID])));
	vec4 position;
	position = modelViewMatrix * vec4(VS_position,1.0);
	viewSpacePosition = position.xyz;
	viewSpaceNormal = normalMatrix * VS_normal;
	texCoord = VS_texCoord;
	texLayer = VS_drawParameters.y;
	gl_ViewportIndex = gl_InstanceID;
	gl_Position = projectionMatrices[gl_InstanceID] * position;
}