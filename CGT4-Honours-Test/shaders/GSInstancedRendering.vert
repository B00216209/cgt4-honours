// GSInstancedRendering.vert, a default vertex shader for use with GS indexed viewport multi-view rendering
#version 440

layout (location = 0) in vec3 VS_position;
layout (location = 1) in vec3 VS_normal;
layout (location = 2) in vec2 VS_texCoord;
layout (location = 3) in uvec2 VS_drawParameters;

smooth out vec3 objectSpaceNormalArray;
smooth out vec2 texCoordArray;
flat out uvec2 drawParametersArray;
flat out int instanceID;

void main() 
{
	objectSpaceNormalArray = VS_normal;
	texCoordArray = VS_texCoord;
	drawParametersArray = VS_drawParameters;
	instanceID = gl_InstanceID;
	gl_Position = vec4(VS_position,1.0);
}