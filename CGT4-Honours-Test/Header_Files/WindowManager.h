#pragma once
#include <iostream>
#include <omp.h>
#include "GL/glew.h"
#include "SDL.h"
#include "ServiceManager.h"

/**
*	Function to parse OpenGL error messges into a more readable format
*	@author http://www.altdevblogaday.com/2011/06/23/improving-opengl-error-messages/
*/
void FormatDebugOutputARB(char outStr[], size_t outStrSize, GLenum source, GLenum type, GLuint id,
	GLenum severity, const char *msg);
/**
*	OpenGL debug error callback function
*	@author http://www.altdevblogaday.com/2011/06/23/improving-opengl-error-messages/
*/
void DebugCallbackARB(GLenum source, GLenum type, GLuint id, GLenum severity,
	GLsizei length, const GLchar *message, GLvoid *userParam);

/**
*	A class which handles windowing and OpenGL context functionality. It is 
*	also responsible for detecting the system threading information and other
*	general system information such as operating system and reported system RAM
*	@author B00216209
*/
class WindowManager
{
public:

	/**
	*	Default constructor
	*	@param p_serviceManager A std::shared_ptr to the ServiceManager
	*/
	WindowManager(std::shared_ptr<ServiceManager> p_serviceManager);

	/**
	*	Default destructor, also calls SDL_GL_DeleteContext(m_glcontext)
	*/
	~WindowManager();

	/**
	*	Outputs information on the system specifications and sets the max thread count
	*	for the OpenMP functionality. It also creates the SDL window and initialises the
	*	OpenGL context.
	*	@return An integer representing the success state of the function, negative values are errors
	*/
	int init();

	/**
	*	A const getter function for astd::shared_ptr to the SDL_Window object
	*	@return The std::shared_ptr<SDL_Window> m_SDL_Window
	*/
	std::shared_ptr<SDL_Window> getWindow() const;

private:

	std::shared_ptr<ServiceManager> m_serviceManager; /**< A std::shared_ptr to the ServiceManager, used to access other managers */
	std::shared_ptr<SDL_Window> m_SDL_Window; /**< A std::shared_ptr to the SDl_Window */
	SDL_GLContext m_glcontext; /**< The SDL OpenGL context object */

	/**
	*	A static initialisation function for the Simple Direct Media libraries video subsystem
	*	@return An integer representing the success state of the function, negative values are errors
	*/
	static int initSDLVideo();

	/**
	*	Initialises the SDL window object m_SDL_Window, a 24-bit depth and doublebuffering is requested
	*	as well as a hardcoded 1280x720 resolution and support for a SDL_GLContext to be bound to it.
	*	@return An integer representing the success state of the function, negative values are errors
	*/
	int createSDLWindow();

	/**
	*	Creates an OpenGL context, if called in a Debug build a debug context will be requested, in both
	*	debug and non-debug contexts a Core profile is requested. The context is then made current and attached
	*	to the SDL window. Glew is then used to wrangle the OpenGL function pointers as well as check that
	*	the requested context is at least the minimum version required for program functionality. Some misc
	*	info on the GPU and OpenGL context is then outputed. V-sync is requested to be off to enable
	*	more accurate profiling. Lastly if a debug context was requested a debug message callback function
	*	is set to aid in OpenGL debugging.
	*	@return An integer representing the success state of the function, negative values are errors
	*/
	int initOpenGLContext();
};

