#pragma once
#include <unordered_map>
#include <vector>
#include "ServiceManager.h"
#include "RendererManager.h"
#include "DynamicCubeScene.h"

/**
*	A class which serves as a Manager for AbstractScene subclasses. It provides
*	a simple interface for initialising and swapping between active scenes.
*	@author B00216209
*/
class SceneManager
{
public:

	/**
	*	Default constructor
	*	@param p_serviceManager A std::shared_ptr to the ServiceManager
	*/
	SceneManager(std::shared_ptr<ServiceManager> p_serviceManager);

	/**
	*	Default destructor
	*/
	~SceneManager();

	/**
	*	Compares the given name to names in m_sceneNames, if a match is found the old current
	*	scene is cleaped up then the new current scene is set by using the match index as a lookup into m_scenePointers.
	*	The new current scene is then initialised.
	*	@param p_sceneName A const std::string& used to compare with existing names
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int setCurrentScene(const std::string& p_sceneName);

	/**
	*	Swaps the current scene to scene at the next index, this will wrap around to 0 if the index
	*	becomes greater than the scene count + 1
	*/
	void goToNextScene();

	/**
	*	Swaps the current scene to scene at the previous index, this will wrap around to 
	*	scene count + 1 if the index becomes less than 0
	*/
	void goToPreviousScene();

	/**
	*	A const getter function for a const std::shared_ptr to the current scene
	*	@return A std::shared_ptr<AbstractScene> for the current scene
	*/
	const std::shared_ptr<AbstractScene> getCurrentScenePointer() const;

	/**
	*	Initialises each AbstractScene concrete subclass and if sucessful it is pushed into the m_scenePointers
	*	and m_sceneNames vectors. If any are unsucessful the function will ouput an error message and return -1
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int init();
private:
	std::shared_ptr<ServiceManager> m_serviceManager; /**< A std::shared_ptr to the ServiceManager, used to access other managers */
	std::size_t m_currentSceneIndex; /**< A std::size_t representing the current scene index */
	std::vector<std::string> m_sceneNames; /**< A vector of std::string used to generate an index from a matched scene name */
	std::vector<std::shared_ptr<AbstractScene>> m_scenePointers; /**< A vector of std::shared_ptr to AbstractScene, used to store them and for swapping current scenes */
	std::shared_ptr<AbstractScene> m_currentScenePointer; /**< A std::shared_ptr to the AbstractScene which is the current scene. */
};

