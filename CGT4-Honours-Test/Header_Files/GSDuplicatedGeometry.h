#pragma once
#include "AbstractRenderer.h"

/**
*	A concrete subclass of AbstractRenderer which renders the scene by geometry
*	shader duplication.
*	@author B00216209
*/
class GSDuplicatedGeometry : public AbstractRenderer
{
public:

	/**
	*	Default constructor
	*	@param p_serviceManager A std::shared_ptr to the ServiceManager
	*/
	GSDuplicatedGeometry(std::shared_ptr<ServiceManager> p_serviceManager);
	
	/**
	*	Default destructor, calls cleanUpScene() if the scene has been initialised.
	*/
	~GSDuplicatedGeometry();
	
	/**
	*	Initialises the shaders, Initialises the viewport array and initialises the view and projection
	*	matrices.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int init();
	
	/**
	*	If the scene has already been initialised, call cleanUpScene before re-initialising via sceneSetup()
	*	@param p_currentScene const std::shared_ptr<AbstractScene>& to the current scene, used to access the scene information.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int initScene(const std::shared_ptr<AbstractScene>& p_currentScene);
	
	/**
	*	Calls sceneCLeanUp()
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int cleanUpScene();
	
	/**
	*	Sets the OpenGL global state to what is needed for this renderer
	*/
	void refresh();
	
	/**
	*	Renders the scene using renderViewportArray()
	*/
	void render();
};
