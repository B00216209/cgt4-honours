#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <memory>
#include <GL/glew.h>

#include "ServiceManager.h"

/**
*	A manager class for shader programs.
*	@author B00216209
*/
class ShaderManager
{
public:

	/**
	*	Default Constructor
	*	Loads a few hard-coded shader names and sets some texture uniform binding points. In future this will all be done via config file variables.
	*	@param p_serviceManager A std::shared_ptr to the ServiceManager
	*/
	ShaderManager(std::shared_ptr<ServiceManager> p_serviceManager);

	/**
	*	Default Destructor
	*/
	~ShaderManager();

	/**
	*	Loads a new 2 component (vertex + Fragment) shader program.
	*	Creates a vertex and fragment shader, loads the file, compiles the shaders, attaches the shaders to a program object, sets bind attribute locations, links the shader program and adds it to the map.
	*	@param p_vertName A const std::string reference containing the name of the vertex shader.
	*	@param p_fragName A const std::string reference containing the name of the fragment shader.
	*	@return A GLuint OpenGL handle to the program object
	*/
	static GLuint loadGLShader(const std::string& p_vertName, const std::string& p_fragName);

	/**
	*	Loads a new 3 component (vertex + Geometry + Fragment) shader program.
	*	Creates a vertex shader, geomtry shader and fragment shader, loads the file, compiles the shaders, attaches the shaders to a program object, sets bind attribute locations, links the shader program and adds it to the map.
	*	@param p_vertName A const std::string reference containing the name of the vertex shader.
	*	@param p_geoName A const std::string reference containing the name of the geometry shader.
	*	@param p_fragName A const std::string reference containing the name of the fragment shader.
	*	@return A GLuint OpenGL handle to the program object
	*/
	static GLuint loadGLShader(const std::string& p_vertName, const std::string& p_geoName, const std::string& p_fragName);

private:

	/**
	*	Loads a file and returns a const char* of the contents
	*	@param p_fname A const std::string reference containing the path to the file to load
	*	@param p_size A Glint reference to store the size of the const char* returned by the function
	*	@return A const char* representation of the loaded file
	*/
	static const char* loadFile(const std::string& p_fname, GLint& p_size);

	/**
	*	Prints a shader Error log.
	*	@param A const GLuint OpenGL shader or program handle used to print it's error log if one exists.
	*	@author Daniel Livingstone
	*/
	static void printShaderError(const GLuint p_shader);

	std::shared_ptr<ServiceManager> m_ServiceManager; /**< A std::shared_ptr to the ServiceManager, used to access other managers */
};
