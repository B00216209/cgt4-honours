#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/**
*	An abstract base class which contains all of the scene information that a
*	renderer needs in order to render the scene and allows the SceneManager class
*	to initialise and clean up the concrete subclasses by their implementation of
*	the pure virtual functions init() and cleanUP()
*	@author B00216209
*/
class AbstractScene
{
public:

	/**
	*	Default constructor
	*/
	AbstractScene() : m_renderableObjectCount(0), m_vertexCount(0), m_indexCount(0), m_textureCount(0){};
	
	/**
	*	Pure virtual destructor to prevent class instantiation
	*/
	virtual ~AbstractScene() = 0;

	/**
	*	Pure virtual initialisation function used as an interface to allow
	*	subclasses to have their own implementations while still being able to
	*	call it via a pointer to this abstract base class.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	virtual int init() = 0;

	/**
	*	Pure virtual clean up function used as an interface to allow
	*	subclasses to have their own implementations while still being able to
	*	call it via a pointer to this abstract base class.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	virtual int cleanUP() = 0;

	/**
	*	A const getter function for a const reference to the const aiScene pointer vector
	*	@return A const reference to m_aiSceneVector
	*/
	const std::vector<const aiScene*>& getSceneVector() const;

	/**
	*	A const getter function for a const reference to the glm::vec4 position vector
	*	@return A const reference to m_positionVector
	*/
	const std::vector<glm::vec4>& getPositionVector() const;

	/**
	*	A const getter function for a const reference to the glm::fquat orientation vector
	*	@return A const reference to m_orientationVector
	*/
	const std::vector<glm::fquat>& getOrientationVector() const;

	/**
	*	A const getter function for a const reference to the glm::mat4 model matrix vector
	*	@return A const reference to m_modelMatrixVector
	*/
	const std::vector<glm::mat4>& getModelMatrixVector() const;

	/**
	*	A const getter function for a const reference to the GLuint per mesh instance count vector
	*	@return A const reference to m_perMeshInstanceCount
	*/
	const std::vector<GLuint>& getperMeshInstanceCountVector() const;

	/**
	*	A const getter function for a const reference to the std::string texture file name vector
	*	@return A const reference to m_textureFilenameVector
	*/
	const std::vector<std::string>& getTextureFilenameVector() const;

	/**
	*	A const getter function for a const reference to the GLuint renderable object texture ID vector
	*	@return A const reference to m_renderableObjectTextureIDVector
	*/
	const std::vector<GLuint>& getRenderableObjectTextureIDVector() const;

	/**
	*	A const getter function for the GLuint renderable object count variable
	*	@return A by-value GLuint representing m_renderableObjectCount
	*/
	GLuint getRenderableObjectCount() const;

	/**
	*	A const getter function for the GLuint total scene vertex count variable
	*	@return A by-value GLuint representing m_vertexCount
	*/
	GLuint getVertexCount() const;

	/**
	*	A const getter function for the GLuint total scene vertex index count variable
	*	@return A by-value GLuint representing m_indexCount
	*/
	GLuint getIndexCount() const;

	/**
	*	A const getter function for the GLuint texture count variable
	*	@return A by-value GLuint representing m_textureCount
	*/
	GLuint getTextureCount() const;

protected:

	/**
	*	Takes a file path as input then loads the model file	and parses it into a const aiScene 
	*	pointer which is inserted into the m_aiSceneVector variable
	*	@param p_filePath The path to the model file. Uses Linux style paths e.g. "models/testModel.obj"
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int importFiletoAiScene(const std::string& p_filePath);

	std::vector<const aiScene*> m_aiSceneVector; /**< A vector of const aiScene pointers */
	std::vector<glm::vec4> m_positionVector; /**< A vector of renderable object positions */
	std::vector<glm::fquat> m_orientationVector; /**< A vector of renderable object orientations */
	std::vector<glm::mat4> m_modelMatrixVector; /**< A vector of renderable object model matrices */
	std::vector<GLuint> m_perMeshInstanceCount; /**< A vector of per-mesh instance counts */
	std::vector<std::string> m_textureFilenameVector; /**< A vector of texture file names */
	std::vector<GLuint> m_renderableObjectTextureIDVector; /**< A vector of renderable object texture IDs */
	GLuint m_renderableObjectCount; /**< A GLuint representing the total renderable object count */
	GLuint m_vertexCount; /**< A GLuint representing the total scene vertex count */
	GLuint m_indexCount; /**< A GLuint representing the total scene vertex index count */
	GLuint m_textureCount; /**< A GLuint representing the total texture count, equal to m_textureFilenameVector.size() */
};
