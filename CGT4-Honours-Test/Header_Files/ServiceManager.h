#pragma once
#include <memory>

class WindowManager;
class RendererManager;
class SceneManager;
class ShaderManager;

/**
*	A class which serves as a container for all other Manager classes to allow for
*	easy communication between managers
*	@author B00216209
*/
class ServiceManager
{
public:

	/**
	*	Default constructor
	*/
	ServiceManager();

	/**
	*	Default destructor
	*/
	~ServiceManager();

	/**
	*	A const getter function for a std::shared_ptr to the WindowManager
	*	@return A std::shared_ptr to the WindowManager
	*/
	std::shared_ptr<WindowManager> getWindowManager() const;

	/**
	*	A setter function for m_windowManager
	*	@param p_windowManager A std::shared_ptr to the WindowManager
	*/
	void setWindowManager(std::shared_ptr<WindowManager> p_windowManager);

	/**
	*	A const getter function for a std::shared_ptr to the RenderManager
	*	@return A std::shared_ptr to the RenderManager
	*/
	std::shared_ptr<RendererManager> getRendererManager() const;

	/**
	*	A setter function for m_rendererManager
	*	@param p_renderManager A std::shared_ptr to the RendererManager
	*/
	void setRendererManager(std::shared_ptr<RendererManager> p_renderManager);

	/**
	*	A const getter function for a std::shared_ptr to the SceneManager
	*	@return A std::shared_ptr to the SceneManager
	*/
	std::shared_ptr<SceneManager> getSceneManager() const;

	/**
	*	A setter function for m_sceneManager
	*	@param p_windowManager A std::shared_ptr to the SceneManager
	*/
	void setSceneManager(std::shared_ptr<SceneManager> p_sceneManager);

	/**
	*	A const getter function for a std::shared_ptr to the ShaderManager
	*	@return A std::shared_ptr to the ShaderManager
	*/
	std::shared_ptr<ShaderManager> getShaderManager() const;

	/**
	*	A setter function for m_shaderManager
	*	@param p_windowManager A std::shared_ptr to the ShaderManager
	*/
	void setShaderManager(std::shared_ptr<ShaderManager> p_shaderManager);

private:
	
	std::shared_ptr<WindowManager> m_windowManager; /**< A std::shared_ptr to the WindowManager */
	std::shared_ptr<RendererManager> m_rendererManager; /**< A std::shared_ptr to the RendererManager */
	std::shared_ptr<SceneManager> m_sceneManager; /**< A std::shared_ptr to the SceneManager */
	std::shared_ptr<ShaderManager> m_shaderManager; /**< A std::shared_ptr to the ShaderManager */
};

