#pragma once
#include <unordered_map>
#include <vector>
#include <memory>
#include "ServiceManager.h"
#include "SceneManager.h"
#include "DuplicateCalls.h"
#include "VSInstancedRendering.h"
#include "GSInstancedRendering.h"
#include "GSDuplicatedGeometry.h"

/**
*	A class which serves as a Manager for AbstractRenderer subclasses. It provides
*	a simple interface for initialising and swapping between active renderers.
*	@author B00216209
*/
class RendererManager
{
public:
	
	/**
	*	Default constructor
	*	@param p_serviceManager A std::shared_ptr to the ServiceManager
	*/
	RendererManager(std::shared_ptr<ServiceManager> p_serviceManager);
	
	/**
	*	Default destructor
	*/
	~RendererManager();
	
	/**
	*	Compares the given name to names in m_rendererNames, if a match is found
	*	the current renderer is set by using the match index as a lookup into m_rendererPointers.
	*	The new current renderer is then refreshed and has scene initialised.
	*	@param p_rendererName A const std::string& used to compare with existing names
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int setCurrentRenderer(const std::string& p_rendererName);
	
	
	/**
	*	Swaps the current renderer to renderer at the next index, this will wrap around to 0 if the index
	*	becomes greater than the renderer count + 1
	*/
	void goToNextRenderer();
	
	/**
	*	Swaps the current renderer to renderer at the previous index, this will wrap around to 
	*	renderer count + 1 if the index becomes less than 0
	*/
	void goToPreviousRenderer();
	
	/**
	*	A const getter function for a const std::shared_ptr to the current renderer
	*	@return A std::shared_ptr<AbstractRenderer> for the current renderer
	*/
	const std::shared_ptr<AbstractRenderer> getCurrentRendererPointer() const;
	
	/**
	*	Initialises each renderer supported by the user's system, Glew is used to check for extension
	*	support for renderers that need them. As long as one renderer successfully initialised the
	* 	function returns 0. Any that are successful have their respective bool set to true to aid 
	*	the benchmarking functionality.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int init();
	
	/**
	*	A const getter function for bool who's value shows the initialisation state of the Vertex Shader
	* 	Indexed renderer.
	*	@return A std::shared_ptr<AbstractRenderer> for the current renderer
	*/
	bool getVSIEnabled() const;
	
	/**
	*	A const getter function for bool who's value shows the initialisation state of the Geometry Shader
	* 	Indexed renderer.
	*	@return A std::shared_ptr<AbstractRenderer> for the current renderer
	*/
	bool getGSIEnabled() const;
	
	/**
	*	A const getter function for bool who's value shows the initialisation state of the Geometry Shader
	* 	duplication renderer.
	*	@return A std::shared_ptr<AbstractRenderer> for the current renderer
	*/
	bool getGSDEnabled() const;
	
	/**
	*	A const getter function for bool who's value shows the initialisation state of the Duplicate Draw
	* 	Calls renderer.
	*	@return A std::shared_ptr<AbstractRenderer> for the current renderer
	*/
	bool getDDCEnabled() const;
	
private:

	std::shared_ptr<ServiceManager> m_serviceManager; /**< A std::shared_ptr to the ServiceManager, used to access other managers */
	std::size_t m_currentRendererIndex; /**< A std::size_t representing the current renderer index */
	std::vector<std::string> m_rendererNames; /**< A vector of std::string used to generate an index from a matched renderer name */
	std::vector<std::shared_ptr<AbstractRenderer>> m_rendererPointers; /**< A vector of std::shared_ptr to the AbstractRenderer used to store them and for swapping current renderers */
	std::shared_ptr<AbstractRenderer> m_currentRendererPointer; /**< A std::shared_ptr to the AbstractRenderer which is the current renderer. */
	bool m_VSIEnabled; /**< A bool who's value shows the initialisation state of the Vertex Shader Indexed renderer. */
	bool m_GSIEnabled; /**< A bool who's value shows the initialisation state of the Geometry Shader Indexed renderer. */
	bool m_GSDEnabled; /**< A bool who's value shows the initialisation state of the Geometry Shader Duplication renderer. */
	bool m_DDCEnabled; /**< A bool who's value shows the initialisation state of the Duplicate Draw Calls renderer. */
};

