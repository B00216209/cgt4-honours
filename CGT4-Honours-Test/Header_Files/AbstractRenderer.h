#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <unordered_map>
#include <chrono>
#include <omp.h>
#include "SDL.h"
#include "SOIL.h"
#include "ServiceManager.h"
#include "AbstractScene.h"
#include "ShaderManager.h"
#include "WindowManager.h"
#include "RenderableObject.h"

/**
*	An abstract base class which contains all of the functions and member variables
*	that a renderer needs in order to render the scene and allows the RendererManager
*	class to initialise, refresh, clean up, and render the concrete subclasses.
*	@author B00216209
*/
class AbstractRenderer
{
public:

	/**
	*	Default constructor
	*	@param p_serviceManager A std::shared_ptr to the ServiceManager
	*/
	AbstractRenderer(std::shared_ptr<ServiceManager> p_serviceManager) : m_renderCallGPUDurationInNanoSeconds(0),
		m_updateCallCPUDurationInNanoseconds(0), m_GPUTimeQuery0(0), m_GPUTimeQuery1(0), m_currentTimerQueryIndex(0), 
		m_vertexArray(0), m_elementArrayBufferObject(0), m_textureArray(0), 
		m_drawIndirectBuffer(0), m_serviceManager(p_serviceManager),
		m_currentShaderKey("None"), m_syncObject0(nullptr), m_syncObject1(nullptr), m_syncObject2(nullptr),
		m_modelMatrixBufferOffset(0), m_modelMatrixBufferSize(0), m_modelMatrixBufferAlginedSize(0), 
		m_GPUModelMatrixBufferPtr(nullptr){};
		
	/**
	*	Pure virtual destructor to prevent class instantiation
	*/
	virtual ~AbstractRenderer() = 0;
	
	/**
	*	Pure virtual initialisation function used as an interface to allow
	*	subclasses to have their own implementations while still being able to
	*	call it via a pointer to this abstract base class.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	virtual int init() = 0;
	
	/**
	*	Pure virtual refresh function used as an interface to allow
	*	subclasses to have their own implementations while still being able to
	*	call it via a pointer to this abstract base class.
	*/
	virtual void refresh() = 0;
	
	/**
	*	Pure virtual scene initialisation function used as an interface to allow
	*	subclasses to have their own implementations while still being able to
	*	call it via a pointer to this abstract base class.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	virtual int initScene(const std::shared_ptr<AbstractScene>& p_currentScene) = 0;
	
	/**
	*	Pure virtual clean up function used as an interface to allow
	*	subclasses to have their own implementations while still being able to
	*	call it via a pointer to this abstract base class.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	virtual int cleanUpScene() = 0;
	
	/**
	*	Update function which adds a small rotation on the y axis to each
	*	renderable object quaternion then updates the m_modelMatrixVector.
	*	OpenMP is used to multi-thread this process. After being updated
	*	the appropriate region of the triple buffered GPU copy of 
	*	m_modelMatrixVector is updated via memcpy with GL fence sync objects
	*	used for synchronisation. This function is profiled using
	*	SDL_GetPerformanceFrequency() and the result stored in the member
	*	variable m_updateCallCPUDurationInNanoseconds for later use.
	*/
	void update();
	
	/**
	*	Pure virtual render function used as an interface to allow
	*	subclasses to have their own implementations while still being able to
	*	call it via a pointer to this abstract base class.
	*	@return An integer representing the success state of the function
	*/
	virtual void render() = 0;
	
	/**
	*	Uses the p_key parameter as a key to m_shaderPrograms and if a matching
	*	GL program handle is found it is made current using glUseProgram().
	*	@param p_key A string used as a key to the m_shaderPrograms std::unordered_map
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int setCurrentShader(const std::string& p_key);
	
	GLint64 m_renderCallGPUDurationInNanoSeconds; /**< GLint64 used to store the GPU time taken for the glMultiDrawElementsIndirect call */
	int64_t m_updateCallCPUDurationInNanoseconds; /**< int_64t used to store the CPU time taken for the update function */
protected:

	/**
	*	Causes the CPU to repeatedly call glClientWaitSync until the GLsync object has been signalled,
	*	this is used to fence the modelMatrix writes preventing new ones while that range of the buffer
	*	is still in use by the glMultiDrawElementsIndirect call.
	*	@param p_syncObject A pointer to a GLsync object used to fence writes to buffers
	*/
	static void waitforSync(GLsync* p_syncObject);
	
	/**
	*	Generates the GLquery objects and uses them once to fill them with dummy starting data to prevent
	*	errors when deleting the objects.
	*/
	void initTimerQueries();
	
	/**
	*	Generates an OpenGL buffer object, binds it, then initialises it.
	*	@param p_dataSizeInBytes GLsizeiptr specifying the buffer size in bytes
	*	@param p_bufferType GLenum specifying the buffer type, e.g. GL_ARRAY_BUFFER
	*	@param p_bufferUse GLenum specifying the buffer usage, e.g. GL_STATIC_DRAW
	*	@return A GLuint OpenGL handle to the buffer
	*/
	static GLuint initGLBuffer(GLsizeiptr p_dataSizeInBytes, GLenum p_bufferType, GLenum p_bufferUse);
	
	/**
	*	Calls initGLBuffer() with a p_bufferUse of GL_ARRAY_BUFFER then sets up the vertex attribute pointer
	*	and divisor before enabling the vertex attribute array
	*	@param p_dataType GLenum specifying the data type of each component in the array
	*	@param p_dataTypeCount GLint specifying the number of components per vertex attribute. Must be 1,2,3 or 4.
	*	@param p_dataSizeInBytes GLsizeiptr specifying the buffer size in bytes
	*	@param p_attribID GLuint specifying the index of the vertex attribute to be modified
	*	@param p_attribDivisor GLuint specifying the number of instances that will pass between updates of the generic attribute at slot index.
	*	@param p_stride GLsizei specifying the byte offset between consecutive generic vertex attributes. If stride is 0, the generic vertex attributes are understood to be tightly packed in the array.
	*	@param p_bufferUse GLenum specifying the buffer usage, e.g. GL_STATIC_DRAW
	*	@return A GLuint OpenGL handle to the buffer
	*/
	static GLuint initGLVertexAttribBuffer(GLenum p_dataType, GLint p_dataTypeCount, GLsizeiptr p_dataSizeInBytes,
		GLuint p_attribID, GLuint p_attribDivisor, GLsizei p_stride, GLenum p_bufferUse);
		
	/**
	*	Initialises the various renderer variables with data from the current scene to prepare for rendering.
	*	@param p_instanceCount GLuint specifying the number of instances to draw each renderable object. A value of 2 is used with viewport indexing.
	*	@param p_currentScene const std::shared_ptr<AbstractScene>& to the current scene, used to access the scene information
	*/	
	void sceneSetup(GLuint p_instanceCount, const std::shared_ptr<AbstractScene>& p_currentScene);
	
	/**
	*	Cleans up renderer variables to prepare for a future sceneSetup() call, this is also called before destroying the renderer to prevent memory leaks.
	*	@param p_instanceCount GLuint specifying the number of instances to draw each renderable object. A value of 2 is used with viewport indexing.
	*	@param p_currentScene const std::shared_ptr<AbstractScene>& to the current scene, used to access the scene information
	*/	
	void sceneCleanUp();
	
	/**
	*	Initialises the m_viewPortArray vector with hardcoded viewport information
	*/
	void initViewportArray();
	
	/**
	*	Initialises the m_viewPorts vector with hardcoded viewport information and initialises m_viewportIndexLocations.
	*/
	void initMultipleViewports();
	
	/**
	*	Initialises the m_viewMatrices and m_projMatrices vectors with hardcoded matrix information and updates the program uniforms with this data.
	*/
	void initViewandProjectionMatrices();
	
	/**
	*	Initialises the m_modelMatrixBuffer as a a persistently mapped triple buffered GL_SHADER_STORAGE_BUFFER aligned to at least a 16 byte boundary.
	*	@param p_currentScene const reference to the current std::shared_ptr to allow access to scene information
	*/	
	void initModelMatrixBuffer(const std::shared_ptr<AbstractScene>& p_currentScene);
	
	/**
	*	Initialises the m_drawIndirectBuffer, vertex attribute buffers, and element array buffer with data from the current scene.
	*	@param p_instanceCount GLuint specifying the number of instances to draw each renderable object. A value of 2 is used with viewport indexing.
	*	@param p_currentScene const reference to the current std::shared_ptr to allow access to scene information
	*/	
	void initVAOandBuffers(GLuint p_instanceCount, const std::shared_ptr<AbstractScene>& p_currentScene);
	
	/**
	*	Generates and binds m_textureArray to GL_TEXTURE0 and sets it's glTexParameters.
	*	@param p_currentScene const reference to the current std::shared_ptr to allow access to scene information
	*/	
	void initTextureArray(const std::shared_ptr<AbstractScene>& p_currentScene);
	
	/**
	*	Generates and binds m_textureArray to GL_TEXTURE0 and sets it's glTexParameters.
	*	@param p_currentScene const reference to the current std::shared_ptr to allow access to scene information
	*/
	int fillTextureArray(const std::shared_ptr<AbstractScene>& p_currentScene);
	
	/**
	*	Populates the m_drawIndirectCommandVector and m_perDrawVariables with data from the current scene then copies that data to the GPU.
	*	@param p_instanceCount GLuint specifying the number of instances to draw each renderable object. A value of 2 is used with viewport indexing.
	*	@param p_currentScene const reference to the current std::shared_ptr to allow access to scene information
	*/
	void fillCommandandPerDrawBuffers(GLuint p_instanceCount, const std::shared_ptr<AbstractScene>& p_currentScene);
	
	/**
	*	Populates the vertex attribute and vertex index buffers with data from the current scene then copies that data to the GPU.
	*	@param p_currentScene const reference to the current std::shared_ptr to allow access to scene information
	*/
	void fillVAObuffers(const std::shared_ptr<AbstractScene>& p_currentScene);
	
	/**
	*	Using a viewport array which is indexed in either the Geometry shader or the Vertex shader, render the scene using glMultiDrawElementsIndirect.
	*	This call is profiled using an OpenGL timer query object and a fence sync object is created to prevent the update function from writing to data
	* 	that is still in use by the glMultiDrawElementsIndirect call until it is complete. After rendering is complete a call is made to swap the front/back buffers.
	*/
	void renderViewportArray();
	
	/**
	*	Using multiple viewports which are swapped after each render call to glMultiDrawElementsIndirect render the scene.
	*	This call is profiled using an OpenGL timer query object and a fence sync object is created to prevent the update function from writing to data
	* 	that is still in use by the glMultiDrawElementsIndirect call until it is complete. After rendering is complete a call is made to swap the front/back buffers.
	*/
	void renderMultipleViewports();
	
	/**
	*	Loads the shader programs from file using a helper function from ShaderManager. If successful it inserts the shader handle and it's key into m_shaderPrograms.
	*	@param p_key GLuint specifying the number of instances to draw each renderable object. A value of 2 is used with viewport indexing.
	*	@param p_vert const std::string reference for a filepath to the vertex shader
	*	@param p_geo const std::string reference for a filepath to the geometry shader, if the value is "" then no geometry shader is loaded.
	*	@param p_frag const std::string reference for a filepath to the fragment shader
	*	@return A GLint that is either the OpenGL program handle or -1 if there was an error.
	*/
	GLint initShader(const std::string& p_key, const std::string& p_vert, const std::string& p_geo, const std::string& p_frag);
	
	/**
	*	Loads the various complexities of shader programs using multiple calls to initShader()
	*	@param p_key GLuint specifying the number of instances to draw each renderable object. A value of 2 is used with viewport indexing.
	*	@param p_lowVert const std::string reference for a filepath to the low complexity vertex shader
	*	@param p_lowGeo const std::string reference for a filepath to the low complexity geometry shader, if the value is "" then no geometry shader is loaded.
	*	@param p_lowFrag const std::string reference for a filepath to the low complexity fragment shader
	*	@param p_midVert const std::string reference for a filepath to the mid complexity vertex shader
	*	@param p_midGeo const std::string reference for a filepath to the mid complexity geometry shader, if the value is "" then no geometry shader is loaded.
	*	@param p_midFrag const std::string reference for a filepath to the mid complexity fragment shader
	*	@param p_highVert const std::string reference for a filepath to the high complexity vertex shader
	*	@param p_highGeo const std::string reference for a filepath to the high complexity geometry shader, if the value is "" then no geometry shader is loaded.
	*	@param p_highFrag const std::string reference for a filepath to the high complexity fragment shader
	*	@return A GLint specifying the success of the function, 0 for success, -1 if there was an error.
	*/
	GLint initShaders(const std::string& p_lowVert, const std::string& p_lowGeo, const std::string& p_lowFrag,
		const std::string& p_midVert, const std::string& p_midGeo, const std::string& p_midFrag,
		const std::string& p_highVert, const std::string& p_highGeo, const std::string& p_highFrag);

	/**
	* A struct used to store the draw parameters for use with glMultiDrawElementsIndirect
	*/
	struct DrawElementsIndirectCommand{
		GLuint count; /**< The number of elements to be rendered */
		GLuint instanceCount; /**< The number of instances of the geometry that should be draw */
		GLuint firstIndex; /**<  The first index to use when drawing the geometry*/
		GLuint baseVertex; /**< Specifies a constant that should be added to each element of indices when choosing elements from the enabled vertex arrays */
		GLuint baseInstance; /**< Specifies the base instance for use in fetching instanced vertex attributes */
	};
	
	GLuint m_GPUTimeQuery0; /**< An OpenGL timer query object handle */
	GLuint m_GPUTimeQuery1; /**< An OpenGL timer query object handle */
	std::vector<GLuint> m_GPUTimerQueries; /**< A vector used to hold the OpenGL timer query object handles */
	size_t m_currentTimerQueryIndex; /**< The current index used for m_GPUTimerQueries, alternates from 0 to 1 each frame */
	GLuint m_vertexArray; /**< An OpenGL handle for the vertex array object */
	std::vector<GLuint> m_vertexBufferObjectVector; /**< A vector used to hold the OpenGL buffer object handles */
	GLuint m_elementArrayBufferObject; /**< An OpenGL handle for the element array buffer object handle */
	GLuint m_textureArray; /**< An OpenGL handle for the 2D array texture */
	GLuint m_modelMatrixBuffer; /**< An OpenGL handle for the GL_SHADER_STORAGE_BUFFER based model matrix buffer object */
	GLuint m_drawIndirectBuffer; /**< An OpenGL handle for the GL_DRAW_INDIRECT_BUFFER buffer object */
	std::vector<DrawElementsIndirectCommand> m_drawIndirectCommandVector; /**< A vector of DrawElementsIndirectCommand structs used for drawing with glMultiDrawElementsIndirect */
	std::vector<glm::uvec2> m_perMeshVariables; /**< A vector of glm::uvec2 which contain the count and firstIndex variables used when constructing DrawElementsIndirectCommand structs */
	std::vector<glm::mat4> m_viewMatrices; /**< A vector of glm::mat4 view matrices used for multi-view rendering */
	std::vector<glm::mat4> m_projMatrices; /**< A vector of glm::mat4 projection matrices used for multi-view rendering */
	std::shared_ptr<ServiceManager> m_serviceManager; /**< A std::shared_ptr to the ServiceManager, used to access other managers */
	std::unordered_map<std::string, GLuint> m_shaderPrograms; /**< A std::unordered_map which associates descriptive string keys to GLuint OpenGL program handles */
	std::string m_currentShaderKey; /**< The descriptive string key associated with the currently bound OpenGL program handle */
	GLsync m_syncObject0; /**< An OpenGL Sync object handle */
	GLsync m_syncObject1; /**< An OpenGL Sync object handle */
	GLsync m_syncObject2; /**< An OpenGL Sync object handle */
	std::vector<glm::mat4> m_modelMatrixVector; /**< A vector of glm::mat4 view matrices, updated in update() on the CPU side before being transferred to the GPU via a persistently mapped buffer */
	std::vector<RenderableObject> m_renderableObjects; /**< A vector of RenderableOjbects which hold the information needed to render an object with OpenGL */
	std::size_t m_modelMatrixBufferOffset; /**< The current offset into the triple buffered m_modelMatrixBuffer */
	std::size_t m_modelMatrixBufferSize; /**<  The size of the data in bytes to be transferred to the m_modelMatrixBuffer */
	std::size_t m_modelMatrixBufferAlginedSize; /**<  The size of the data in bytes to be transferred to the m_modelMatrixBuffer */
	void* m_GPUModelMatrixBufferPtr; /**< m_modelMatrixBufferSize but raised if needed to align on at least a 16 byte boundary, used to set the m_modelMatrixBufferOffset */
	
	/**
	* A struct used to store the parameters needed when calling glViewport
	*/
	struct viewPort{
		GLint x; /**< Left plane of the viewport */
		GLint y; /**< Bottom plane of the viewport */
		GLsizei width; /**< Width of the viewport */
		GLsizei height; /**< Height of the viewport */
		
		/**
		*	viewPort Constructor
		*	@param x GLint Left plane of the viewport
		*	@param y GLint Bottom plane of the viewport
		*	@param width GLsizei Width of the viewport
		*	@param height GLsizei Height of the viewport
		*/
		viewPort(GLint x, GLint y, GLsizei width, GLsizei height) : x(x), y(y), width(width), height(height) {};
	};
	
	std::vector<viewPort> m_viewPorts; /**< A vector of viewports used with initMultipleViewports() and renderMultipleViewports() */
	std::unordered_map<std::string, GLint> m_viewportIndexLocations; /**< A std::unordered_map which associates descriptive string keys to GLuint OpenGL uniform handles */
	std::vector<GLfloat> m_viewPortArray; /**< A vector of GLfloats representing viewports used with initViewportArray() and renderViewportArray() */
	std::vector<glm::uvec2> m_perDrawVariables; /**<  A vector of glm::uvec2 used to specify the modelMatrix and textureID lookup for object drawn using glMultiDrawElementsIndirect */
};
