#pragma once
#include "AbstractScene.h"

/**
*	An AbstractScene implementation that allows for a constructor specified
*	distribution of 36 vert cuboids centered around a 0,0,0 origin.
*	@author B00216209
*/
class DynamicCubeScene : public AbstractScene
{
public:
	/**
	*	Default constructor
	*	@param p_distribution A glm::uvec3 used to specify the cuboid distribution
	*/
	DynamicCubeScene(glm::uvec3 p_distribution);

	/**
	*	Default destructor
	*/
	~DynamicCubeScene();

	/**
	*	Initialises the scene by loading a cuboid model from disk to system memory,
	*	initialising and populating the position, model matrices, and quaternion 
	*	orientation vectors representing the scene objects the distribution, and by setting the texture ID's for each object
	*	@return An integer representing the success state of the function
	*/
	int init();

	/**
	*	Cleans up the scene by calling an Asset Import Library function which frees
	*	the memory used by the aiScene pointers. The rest of the variables owned by
	*	this class are cleaned up via their destrutctors or via the superclass.
	*	@return An integer representing the success state of the function
	*/
	int cleanUP();
private:
	glm::uvec3 m_distribution; /**< A glm::uvec3 representing the cuboid distribution */
};