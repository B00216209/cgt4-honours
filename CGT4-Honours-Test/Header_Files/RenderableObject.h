#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <chrono>

/**
*	An abstraction class that contains all the information needed to render a single
*	object using glMultiDrawElementsIndirect
*	@author B00216209
*/
class RenderableObject
{
public:

	/**
	*	Default constructor
	*	@param p_indexCount A GLuint representing the number of indices used to draw the object
	*	@param p_indexOffset A GLuint representing the first index used to draw the object
	*	@param p_instanceCount A GLuint representing the number of times to draw the object
	*	@param p_textureOffset A GLuint representing the index of the 2D array texture to use for it's texture
	*	@param p_position A glm::vec4 for storing the object's position
	*	@param p_orientation A glm::fquat for storing the object's orientation
	*/
	RenderableObject(GLuint p_indexCount, GLuint p_indexOffset, GLuint p_instanceCount, GLuint p_textureOffset, glm::vec4 p_position, glm::fquat p_orientation);
	
	/**
	*	Default destructor, also calls closeSDL()
	*/
	~RenderableObject();
	
	/**
	*	A const getter function for a GLuint copy of m_indexCount
	*	@return A Gluint copy of m_indexCount
	*/
	GLuint getIndexCount() const;
	
	/**
	*	A const getter function for a GLuint copy of m_indexOffset
	*	@return A Gluint copy of m_indexOffset
	*/
	GLuint getIndexOffset() const;
	
	/**
	*	A const getter function for a GLuint copy of m_instanceCount
	*	@return A Gluint copy of m_instanceCount
	*/
	GLuint getInstanceCount() const;
	
	/**
	*	A const getter function for a GLuint copy of m_textureOffset
	*	@return A Gluint copy of m_textureOffset
	*/
	GLuint getTextureOffset() const;
	
	/**
	*	A const getter function for a glm::vec4 copy of m_position
	*	@return A glm::vec4 copy of m_position
	*/
	glm::vec4 getPosition() const;
	
	/**
	*	A const getter function for a glm::fquat copy of m_orientation
	*	@return A glm::fquat copy of m_position
	*/
	glm::fquat getOrientation() const;
	
	/**
	*	A const getter function for a glm::mat4 copy of m_modelMatrix
	*	@return A glm::mat4 copy of m_position
	*/
	glm::mat4 getModelMatrix() const;
	
	/**
	*	A setter function for m_position
	*	@param p_position A glm::mat4 representing a world-space position
	*/
	void setPosition(glm::vec4 p_position);
	
	/**
	*	A setter function for m_orientation
	*	@param p_position A glm::fquat representing a world-space orientation
	*/
	void setOrientation(glm::fquat p_orientation);
private:
	
	/**
	*	Updates m_modelMatrix using m_position and m_orientation
	*/
	void updateModelMatrix();
	
	GLuint m_indexCount; /**< A GLuint representing the number of indices used to draw the object */
	GLuint m_indexOffset; /**< A GLuint representing the first index used to draw the object */
	GLuint m_instanceCount; /**< A GLuint representing the number of times to draw the object */
	GLuint m_textureOffset; /**< A GLuint representing the index of the 2D array texture to use for it's texture */
	glm::vec4 m_position; /**< A glm::vec4 for storing the object's position */
	glm::fquat m_orientation; /**< A glm::fquat for storing the object's orientation */
	glm::mat4 m_modelMatrix; /**< A glm::mat4 for storing the object's model matrix transform */
};