#pragma once
#include <iostream>
#include <memory>
#include <string>
#include <sstream>
#include <algorithm>
#include "GL/glew.h"
#include "SDL.h"
#include "ServiceManager.h"
#include "WindowManager.h"
#include "SceneManager.h"
#include "RendererManager.h"
#include "ShaderManager.h"

#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/StreamCopier.h"
#include "Poco/Path.h"
#include "Poco/URI.h"
#include "Poco/Exception.h"
#include "Poco/UUID.h"
#include "Poco/UUIDGenerator.h"

/**
*	This class acts as the main loop point. The current renderer's update() and render()
*	functions are called from within gameLoop() which also contains input event handlers to
*	allow the user to change between scenes, shaders, and renderers. When launched with
*	an optional command line parameter the class instead goes into benchmark() mode and cycles 
*	through all possible combinations of scene, shader, and renderer and upon completion submits 
*	the profiling data to a web server via a HTTP POST request.
*	@author B00216209
*/
class Application
{
public:

	/**
	*	Default constructor, initialises the ServiceManager in the initialisation list.
	*/
	Application();
	
	/**
	*	Default destructor, also calls closeSDL()
	*/
	~Application();
	
	/**
	*	Initialises SDL, the WindowManager, SceneManager, ShaderManager and RendererManager and passes
	*	the created objects to the ServiceManager
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int init();
	
	/**
	*	Loops until m_isRunning is set to false by the detection of ALT+F4 or ESC. During the loop it checks
	*	for SDL_KEYUP events that correspond to actions such as changing the scene, shader, or renderer.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int gameLoop();
	
	/**
	*	Gets or creates the information needed to identify a benchmark run then cycles through all combinations
	*	of scene, shader and renderer before formatting the profiling data and submitting it via a HTTP POST request
	*	to an external web storage. Upon completion of the data submission a URL is placed into the clipboard to allow
	*	them to view their results online in an interactable graph.
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	int benchmark();
	
private:
	
	struct BenchmarkData
	{
		BenchmarkData() : m_OS("DefaultOS"), m_UUID("DefaultUUID"),
			m_gpuName("DefaultGPU"), m_shaderName("DefaultShader"), m_objectCount(0),
			m_VSIAverageCPUTime(0), m_VSIAverageGPUTime(0),
			m_GSIAverageCPUTime(0), m_GSIAverageGPUTime(0),
			m_GSDAverageCPUTime(0), m_GSDAverageGPUTime(0),
			m_DDCAverageCPUTime(0), m_DDCAverageGPUTime(0) {}
		std::string m_OS;
		std::string m_UUID;
		std::string m_gpuName;
		std::string m_shaderName;
		int64_t m_objectCount;
		int64_t m_VSIAverageCPUTime;
		int64_t m_VSIAverageGPUTime;
		int64_t m_GSIAverageCPUTime;
		int64_t m_GSIAverageGPUTime;
		int64_t m_GSDAverageCPUTime;
		int64_t m_GSDAverageGPUTime;
		int64_t m_DDCAverageCPUTime;
		int64_t m_DDCAverageGPUTime;
	};
	
	/**
	*	Initialises SDL
	*	@return An integer representing the success state of the function, non-Zero values are errors.
	*/
	static int initSDL();
	
	/**
	*	Closes down SDL, this must be done before program exit to prevent memory leaks.
	*/
	static void closeSDL();
	
	/**
	*	Converts a BenchmarkData struct into a URL parameter string then creates a HTTP POST request via
	*	the Poco library and submits the data to a hard-coded web server address.
	*	@param p_submissionData A const reference to a BenchmarkData struct to submit
	*/
	static void submitBenchmarkData(const BenchmarkData& p_submissionData);
	
	/**
	*	Updates and Renders 2000 frames using the renderer passed into it and averages the profiling results.
	*	@param p_currentRenderer A std::shared_ptr<AbstractRenderer> passed in order to call it's update and render functions
	*	@param p_CPUAverageTime	A int64_t& to store the CPU average result of the benchmark loop in.
	*	@param p_GPUAverageTime A int64_t& to store the GPU average result of the benchmark loop in
	*/
	void benchmarkLoop(std::shared_ptr<AbstractRenderer> p_currentRenderer, int64_t& p_CPUAverageTime, int64_t& p_GPUAverageTime);
	
	/**
	*	Sets the current renderer to the Vertex Shader Indexed Viewport variant and runs the benchmark loop for all
	*	combinations of scene and shader.
	*	@param p_benchmarkDataVector A std::vector<BenchmarkData>& to store the results of the benchmark loop in.
	*/
	void benchmarkLoopVSI(std::vector<BenchmarkData>& p_benchmarkDataVector);
	
	/**
	*	Sets the current renderer to the Geometry Shader Indexed Viewport variant and runs the benchmark loop for all
	*	combinations of scene and shader.
	*	@param p_benchmarkDataVector A std::vector<BenchmarkData>& to store the results of the benchmark loop in.
	*/
	void benchmarkLoopGSI(std::vector<BenchmarkData>& p_benchmarkDataVector);
	
	/**
	*	Sets the current renderer to the Geometry Shader Duplication variant and runs the benchmark loop for all
	*	combinations of scene and shader.
	*	@param p_benchmarkDataVector A std::vector<BenchmarkData>& to store the results of the benchmark loop in.
	*/
	void benchmarkLoopGSD(std::vector<BenchmarkData>& p_benchmarkDataVector);
	
	/**
	*	Sets the current renderer to the Duplicate draw calls variant and runs the benchmark loop for all
	*	combinations of scene and shader.
	*	@param p_benchmarkDataVector A std::vector<BenchmarkData>& to store the results of the benchmark loop in.
	*/
	void benchmarkLoopDDC(std::vector<BenchmarkData>& p_benchmarkDataVector);
	
	std::shared_ptr<ServiceManager> m_serviceManager; /**< A std::shared_ptr to the ServiceManager, used for access to other manager classes */
};

