var searchData=
[
  ['benchmark',['benchmark',['../class_application.html#abfde59c5745d7b462abaf6144fea95a4',1,'Application']]],
  ['benchmarkdata',['BenchmarkData',['../struct_application_1_1_benchmark_data.html#ac32e62770965a5ab46aae596b02fa23b',1,'Application::BenchmarkData']]],
  ['benchmarkloop',['benchmarkLoop',['../class_application.html#a4e4abf63c13b70b746656694ec46a659',1,'Application']]],
  ['benchmarkloopddc',['benchmarkLoopDDC',['../class_application.html#a5a8012bfe9764e5114a5d17c4810a755',1,'Application']]],
  ['benchmarkloopgsd',['benchmarkLoopGSD',['../class_application.html#aab901eebae48f5602ec762fae84269fb',1,'Application']]],
  ['benchmarkloopgsi',['benchmarkLoopGSI',['../class_application.html#a76d7bdcc68f41874f09178ae28053f17',1,'Application']]],
  ['benchmarkloopvsi',['benchmarkLoopVSI',['../class_application.html#a8211e475352512758447fbe34e7db742',1,'Application']]]
];
