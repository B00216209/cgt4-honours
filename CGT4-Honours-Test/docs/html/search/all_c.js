var searchData=
[
  ['m_5faiscenevector',['m_aiSceneVector',['../class_abstract_scene.html#ab755af631c9eca5ac9710aa1e3aebcaa',1,'AbstractScene']]],
  ['m_5fcurrentrendererindex',['m_currentRendererIndex',['../class_renderer_manager.html#ac8201cc767461c5892f289693d65b10e',1,'RendererManager']]],
  ['m_5fcurrentrendererpointer',['m_currentRendererPointer',['../class_renderer_manager.html#a61ab82c2fb65288ad9620d26c60602b3',1,'RendererManager']]],
  ['m_5fcurrentsceneindex',['m_currentSceneIndex',['../class_scene_manager.html#af381d718adf04e92127d956b326ea8f7',1,'SceneManager']]],
  ['m_5fcurrentscenepointer',['m_currentScenePointer',['../class_scene_manager.html#a4521cd6649218585be1af4358f7b6a87',1,'SceneManager']]],
  ['m_5fcurrentshaderkey',['m_currentShaderKey',['../class_abstract_renderer.html#a49547a95866415e438973174c49fdf92',1,'AbstractRenderer']]],
  ['m_5fcurrenttimerqueryindex',['m_currentTimerQueryIndex',['../class_abstract_renderer.html#acfef0192bfa858dce4e9f06b3ed4b0f6',1,'AbstractRenderer']]],
  ['m_5fddcaveragecputime',['m_DDCAverageCPUTime',['../struct_application_1_1_benchmark_data.html#a3891780f0d5d437916fc03b9dbb97f57',1,'Application::BenchmarkData']]],
  ['m_5fddcaveragegputime',['m_DDCAverageGPUTime',['../struct_application_1_1_benchmark_data.html#ad0ee8994052a9596aeb906a5469e65f6',1,'Application::BenchmarkData']]],
  ['m_5fddcenabled',['m_DDCEnabled',['../class_renderer_manager.html#a6efc16ec5d75c4301b2f5bfebd48e7d6',1,'RendererManager']]],
  ['m_5fdistribution',['m_distribution',['../class_dynamic_cube_scene.html#a9250c979998ae90233ee23f6d7e20d32',1,'DynamicCubeScene']]],
  ['m_5fdrawindirectbuffer',['m_drawIndirectBuffer',['../class_abstract_renderer.html#a9519af73636f380b8cbcedaf3492df37',1,'AbstractRenderer']]],
  ['m_5fdrawindirectcommandvector',['m_drawIndirectCommandVector',['../class_abstract_renderer.html#a9b0916d4bc36dc54c7444c95aa2d3d10',1,'AbstractRenderer']]],
  ['m_5felementarraybufferobject',['m_elementArrayBufferObject',['../class_abstract_renderer.html#a40f8d552a855211c90d4d05adc075188',1,'AbstractRenderer']]],
  ['m_5fglcontext',['m_glcontext',['../class_window_manager.html#a77f268e70fbed20fd81f8d818cbc3e34',1,'WindowManager']]],
  ['m_5fgpumodelmatrixbufferptr',['m_GPUModelMatrixBufferPtr',['../class_abstract_renderer.html#aefb6c8f0ef9028ae1302a3ce429900db',1,'AbstractRenderer']]],
  ['m_5fgpuname',['m_gpuName',['../struct_application_1_1_benchmark_data.html#a7d20cefce3229011a6568a64c68db5d8',1,'Application::BenchmarkData']]],
  ['m_5fgputimequery0',['m_GPUTimeQuery0',['../class_abstract_renderer.html#acd3bd05f9d8046ba4b740b6c75672678',1,'AbstractRenderer']]],
  ['m_5fgputimequery1',['m_GPUTimeQuery1',['../class_abstract_renderer.html#a3f30f21f451bcd0ae589327170ff3e13',1,'AbstractRenderer']]],
  ['m_5fgputimerqueries',['m_GPUTimerQueries',['../class_abstract_renderer.html#a5b98789b39545067c51975f0862b260b',1,'AbstractRenderer']]],
  ['m_5fgsdaveragecputime',['m_GSDAverageCPUTime',['../struct_application_1_1_benchmark_data.html#ad740687cc3ae984e7a9890f178c5c683',1,'Application::BenchmarkData']]],
  ['m_5fgsdaveragegputime',['m_GSDAverageGPUTime',['../struct_application_1_1_benchmark_data.html#a6e6cd09ab10a2c88e9aceac2e0c2d1f4',1,'Application::BenchmarkData']]],
  ['m_5fgsdenabled',['m_GSDEnabled',['../class_renderer_manager.html#a69c4029510daf2e634f439e46bb9a8d8',1,'RendererManager']]],
  ['m_5fgsiaveragecputime',['m_GSIAverageCPUTime',['../struct_application_1_1_benchmark_data.html#a8deeb8a2581f2c38a6bdbc551128f973',1,'Application::BenchmarkData']]],
  ['m_5fgsiaveragegputime',['m_GSIAverageGPUTime',['../struct_application_1_1_benchmark_data.html#a235fc8bfeb8ba4d425a081d20b77ab65',1,'Application::BenchmarkData']]],
  ['m_5fgsienabled',['m_GSIEnabled',['../class_renderer_manager.html#a9cf268bcdfb2fbf60284f5b689661def',1,'RendererManager']]],
  ['m_5findexcount',['m_indexCount',['../class_abstract_scene.html#a96ea1ab8d12cec5bbd47430bb9dd7e14',1,'AbstractScene::m_indexCount()'],['../class_renderable_object.html#a7e741f99428e30558de91bf5179f8297',1,'RenderableObject::m_indexCount()']]],
  ['m_5findexoffset',['m_indexOffset',['../class_renderable_object.html#a99ab59a5ff3625bfe0ac9bba924f7f74',1,'RenderableObject']]],
  ['m_5finstancecount',['m_instanceCount',['../class_renderable_object.html#a10e980024336f0e9094c5c1d37c1373d',1,'RenderableObject']]],
  ['m_5fmodelmatrix',['m_modelMatrix',['../class_renderable_object.html#a3cca598db353f1db5f86eea997c4b4e9',1,'RenderableObject']]],
  ['m_5fmodelmatrixbuffer',['m_modelMatrixBuffer',['../class_abstract_renderer.html#a15db03614ea11dc5e747176d78730fc7',1,'AbstractRenderer']]],
  ['m_5fmodelmatrixbufferalginedsize',['m_modelMatrixBufferAlginedSize',['../class_abstract_renderer.html#ab296d04c069927ca88ac1cb1176aa712',1,'AbstractRenderer']]],
  ['m_5fmodelmatrixbufferoffset',['m_modelMatrixBufferOffset',['../class_abstract_renderer.html#a237df3c7bef3c8a7777dfcfd707147a2',1,'AbstractRenderer']]],
  ['m_5fmodelmatrixbuffersize',['m_modelMatrixBufferSize',['../class_abstract_renderer.html#a92347f9c2b802a11138945f0744d7d99',1,'AbstractRenderer']]],
  ['m_5fmodelmatrixvector',['m_modelMatrixVector',['../class_abstract_renderer.html#a62b0dcccd9b33bfa73fb622ff9955090',1,'AbstractRenderer::m_modelMatrixVector()'],['../class_abstract_scene.html#aece0c3dbe99b1bcad06eb0149f7ac5b1',1,'AbstractScene::m_modelMatrixVector()']]],
  ['m_5fobjectcount',['m_objectCount',['../struct_application_1_1_benchmark_data.html#a223989983445ec424ed2c7542bc6f01e',1,'Application::BenchmarkData']]],
  ['m_5forientation',['m_orientation',['../class_renderable_object.html#af149b4121b7f68333b45e891764291d3',1,'RenderableObject']]],
  ['m_5forientationvector',['m_orientationVector',['../class_abstract_scene.html#ab4689e8d77b3743b41da5df0d473da2d',1,'AbstractScene']]],
  ['m_5fos',['m_OS',['../struct_application_1_1_benchmark_data.html#a3f868af78c772fc7d733fdfae4671312',1,'Application::BenchmarkData']]],
  ['m_5fperdrawvariables',['m_perDrawVariables',['../class_abstract_renderer.html#a265527854eee1f343e0e4e5640793818',1,'AbstractRenderer']]],
  ['m_5fpermeshinstancecount',['m_perMeshInstanceCount',['../class_abstract_scene.html#a5bc4d7fc352fc8f7e1c37fafaf3f6a97',1,'AbstractScene']]],
  ['m_5fpermeshvariables',['m_perMeshVariables',['../class_abstract_renderer.html#a2f5152e12c1b2635ef9e61f8e67e341a',1,'AbstractRenderer']]],
  ['m_5fposition',['m_position',['../class_renderable_object.html#aa96c6acd0b5913d78414a3af0b752261',1,'RenderableObject']]],
  ['m_5fpositionvector',['m_positionVector',['../class_abstract_scene.html#a1b444ca212be7d668d921e33559ff834',1,'AbstractScene']]],
  ['m_5fprojmatrices',['m_projMatrices',['../class_abstract_renderer.html#af9dba757a0b0e025dbbd1a5c6dca849e',1,'AbstractRenderer']]],
  ['m_5frenderableobjectcount',['m_renderableObjectCount',['../class_abstract_scene.html#a364fecdd83ba420b78c4f661a35f3146',1,'AbstractScene']]],
  ['m_5frenderableobjects',['m_renderableObjects',['../class_abstract_renderer.html#af1db3af437e630994004b3c70089c443',1,'AbstractRenderer']]],
  ['m_5frenderableobjecttextureidvector',['m_renderableObjectTextureIDVector',['../class_abstract_scene.html#abfa8b70d73114a82582e02c0222e677e',1,'AbstractScene']]],
  ['m_5frendercallgpudurationinnanoseconds',['m_renderCallGPUDurationInNanoSeconds',['../class_abstract_renderer.html#a45482718ad1fed1d6e46dec0d4ead653',1,'AbstractRenderer']]],
  ['m_5frenderermanager',['m_rendererManager',['../class_service_manager.html#ae97b83216672cadf8f3f430c11ac28cb',1,'ServiceManager']]],
  ['m_5frenderernames',['m_rendererNames',['../class_renderer_manager.html#af1eb54f88fa37b24271ba36f67c85835',1,'RendererManager']]],
  ['m_5frendererpointers',['m_rendererPointers',['../class_renderer_manager.html#a54c19c7b1743be2bdf16cb0462a4a146',1,'RendererManager']]],
  ['m_5fscenemanager',['m_sceneManager',['../class_service_manager.html#ad4ff4e778a41375068db4313b346cbfc',1,'ServiceManager']]],
  ['m_5fscenenames',['m_sceneNames',['../class_scene_manager.html#a701c9f71d2cb4777aca6b4aed1969f05',1,'SceneManager']]],
  ['m_5fscenepointers',['m_scenePointers',['../class_scene_manager.html#a6e8337e82cb4d9090ef57a2aa1554446',1,'SceneManager']]],
  ['m_5fsdl_5fwindow',['m_SDL_Window',['../class_window_manager.html#af42c2921a295d2c58e382db6b0f47294',1,'WindowManager']]],
  ['m_5fservicemanager',['m_serviceManager',['../class_abstract_renderer.html#a01fce180d19f468af7bc77db7abb3230',1,'AbstractRenderer::m_serviceManager()'],['../class_application.html#a127b902460420813852f83970efa3338',1,'Application::m_serviceManager()'],['../class_renderer_manager.html#adfb4e1abf33b4eeece23a26bcddbe673',1,'RendererManager::m_serviceManager()'],['../class_scene_manager.html#aa235ebbd7bd4b6f7a5ce6e4aaaa74595',1,'SceneManager::m_serviceManager()'],['../class_window_manager.html#ac7d928bb8848fa29adba03e5ac3d73e4',1,'WindowManager::m_serviceManager()'],['../class_shader_manager.html#abae0c7457506902fb4d64045684097cd',1,'ShaderManager::m_ServiceManager()']]],
  ['m_5fshadermanager',['m_shaderManager',['../class_service_manager.html#aa4d60a8af6e50808c9d3ddcd1e295455',1,'ServiceManager']]],
  ['m_5fshadername',['m_shaderName',['../struct_application_1_1_benchmark_data.html#ac4178121907e4a937c5eb3abd9209218',1,'Application::BenchmarkData']]],
  ['m_5fshaderprograms',['m_shaderPrograms',['../class_abstract_renderer.html#a77f4f5c39690b4453c368d56801abe31',1,'AbstractRenderer']]],
  ['m_5fsyncobject0',['m_syncObject0',['../class_abstract_renderer.html#ada95b050b6e7d4db7ec02d9b314c91ca',1,'AbstractRenderer']]],
  ['m_5fsyncobject1',['m_syncObject1',['../class_abstract_renderer.html#a2e946f11b61c2963dd19a8cfbcd36cbb',1,'AbstractRenderer']]],
  ['m_5fsyncobject2',['m_syncObject2',['../class_abstract_renderer.html#a500104c9704cc2391cdfcad823b03858',1,'AbstractRenderer']]],
  ['m_5ftexturearray',['m_textureArray',['../class_abstract_renderer.html#af0deb37b0c3d19b6bee73a77f0b3ba82',1,'AbstractRenderer']]],
  ['m_5ftexturecount',['m_textureCount',['../class_abstract_scene.html#a91018b4d72d2f6924f2d7a0ee65c8e13',1,'AbstractScene']]],
  ['m_5ftexturefilenamevector',['m_textureFilenameVector',['../class_abstract_scene.html#a1a9b688cfde41fa7e20fd8e292852a4a',1,'AbstractScene']]],
  ['m_5ftextureoffset',['m_textureOffset',['../class_renderable_object.html#aa7995469374e3b5ae05d8adf5c8b0f47',1,'RenderableObject']]],
  ['m_5fupdatecallcpudurationinnanoseconds',['m_updateCallCPUDurationInNanoseconds',['../class_abstract_renderer.html#ad80305dceed7bfdc23554863d0799b20',1,'AbstractRenderer']]],
  ['m_5fuuid',['m_UUID',['../struct_application_1_1_benchmark_data.html#a3b0ad2e5358b7e2c645c6dbc7ccdfe3e',1,'Application::BenchmarkData']]],
  ['m_5fvertexarray',['m_vertexArray',['../class_abstract_renderer.html#ac0efcb6af5ed646563b1eac7138a71da',1,'AbstractRenderer']]],
  ['m_5fvertexbufferobjectvector',['m_vertexBufferObjectVector',['../class_abstract_renderer.html#a5e65353734d24f8f03a9d1bd30087c56',1,'AbstractRenderer']]],
  ['m_5fvertexcount',['m_vertexCount',['../class_abstract_scene.html#a0ac07f198265c54879de3f4b5ca79ada',1,'AbstractScene']]],
  ['m_5fviewmatrices',['m_viewMatrices',['../class_abstract_renderer.html#a2d7fbb3fce46cc286c83dc4e2be6c847',1,'AbstractRenderer']]],
  ['m_5fviewportarray',['m_viewPortArray',['../class_abstract_renderer.html#aafd84ed8629b6c9d39e59a10124e060f',1,'AbstractRenderer']]],
  ['m_5fviewportindexlocations',['m_viewportIndexLocations',['../class_abstract_renderer.html#a8e67421e4ea833c422942bbffcb72546',1,'AbstractRenderer']]],
  ['m_5fviewports',['m_viewPorts',['../class_abstract_renderer.html#aa976e3fc28ce7b6637099ed1fb0266a1',1,'AbstractRenderer']]],
  ['m_5fvsiaveragecputime',['m_VSIAverageCPUTime',['../struct_application_1_1_benchmark_data.html#a66fed3b1cc2eef342cbc9f3675be9e6c',1,'Application::BenchmarkData']]],
  ['m_5fvsiaveragegputime',['m_VSIAverageGPUTime',['../struct_application_1_1_benchmark_data.html#af4fbd1a2fe6d6f2c442856e2282ba96a',1,'Application::BenchmarkData']]],
  ['m_5fvsienabled',['m_VSIEnabled',['../class_renderer_manager.html#a095bf329d282627f60672a257165e0f5',1,'RendererManager']]],
  ['m_5fwindowmanager',['m_windowManager',['../class_service_manager.html#a7f1ab6982f39e3b66a97a096cba887c0',1,'ServiceManager']]],
  ['main',['main',['../main_8cpp.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main.cpp']]],
  ['main_2ecpp',['main.cpp',['../main_8cpp.html',1,'']]],
  ['max_5fkey',['max_key',['../class_command_line_processing_1_1_argv_parser.html#abf9caab1dfd8acf2695b5fda4eef3e4e',1,'CommandLineProcessing::ArgvParser']]]
];
