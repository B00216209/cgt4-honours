var class_window_manager =
[
    [ "WindowManager", "class_window_manager.html#a5ce0001d93a214f504f516e68fc0b8c9", null ],
    [ "~WindowManager", "class_window_manager.html#a19fd6e41c42760af82460d9851780d82", null ],
    [ "createSDLWindow", "class_window_manager.html#a73d845db9a27414f4c12b8442fb36b56", null ],
    [ "getWindow", "class_window_manager.html#ad92272bce0d4b46424bfc4d7afe05151", null ],
    [ "init", "class_window_manager.html#a356252328f891e692eeeb4a097778fef", null ],
    [ "initOpenGLContext", "class_window_manager.html#a676ebfabf543534491b9c70918f2f00e", null ],
    [ "initSDLVideo", "class_window_manager.html#adfc1fc8793abfc57f5039a068bd8eee3", null ],
    [ "m_glcontext", "class_window_manager.html#a77f268e70fbed20fd81f8d818cbc3e34", null ],
    [ "m_SDL_Window", "class_window_manager.html#af42c2921a295d2c58e382db6b0f47294", null ],
    [ "m_serviceManager", "class_window_manager.html#ac7d928bb8848fa29adba03e5ac3d73e4", null ]
];