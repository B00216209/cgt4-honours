var class_application =
[
    [ "BenchmarkData", "struct_application_1_1_benchmark_data.html", "struct_application_1_1_benchmark_data" ],
    [ "Application", "class_application.html#afa8cc05ce6b6092be5ecdfdae44e05f8", null ],
    [ "~Application", "class_application.html#a748bca84fefb9c12661cfaa2f623748d", null ],
    [ "benchmark", "class_application.html#abfde59c5745d7b462abaf6144fea95a4", null ],
    [ "benchmarkLoop", "class_application.html#a4e4abf63c13b70b746656694ec46a659", null ],
    [ "benchmarkLoopDDC", "class_application.html#a5a8012bfe9764e5114a5d17c4810a755", null ],
    [ "benchmarkLoopGSD", "class_application.html#aab901eebae48f5602ec762fae84269fb", null ],
    [ "benchmarkLoopGSI", "class_application.html#a76d7bdcc68f41874f09178ae28053f17", null ],
    [ "benchmarkLoopVSI", "class_application.html#a8211e475352512758447fbe34e7db742", null ],
    [ "closeSDL", "class_application.html#a180cda02a95bc5f8ce341fe4f89703fb", null ],
    [ "gameLoop", "class_application.html#a3cccee3c10b46727e07d403d0ae75916", null ],
    [ "init", "class_application.html#a1532f2869d8242ed3b097f0e8cb80baa", null ],
    [ "initSDL", "class_application.html#adfd2583066a6f2d13ce53a0af8c28953", null ],
    [ "submitBenchmarkData", "class_application.html#aba4afdb3a540bbf48f8ccaa01a22e9df", null ],
    [ "m_serviceManager", "class_application.html#a127b902460420813852f83970efa3338", null ]
];