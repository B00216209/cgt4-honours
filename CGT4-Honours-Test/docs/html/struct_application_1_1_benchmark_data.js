var struct_application_1_1_benchmark_data =
[
    [ "BenchmarkData", "struct_application_1_1_benchmark_data.html#ac32e62770965a5ab46aae596b02fa23b", null ],
    [ "m_DDCAverageCPUTime", "struct_application_1_1_benchmark_data.html#a3891780f0d5d437916fc03b9dbb97f57", null ],
    [ "m_DDCAverageGPUTime", "struct_application_1_1_benchmark_data.html#ad0ee8994052a9596aeb906a5469e65f6", null ],
    [ "m_gpuName", "struct_application_1_1_benchmark_data.html#a7d20cefce3229011a6568a64c68db5d8", null ],
    [ "m_GSDAverageCPUTime", "struct_application_1_1_benchmark_data.html#ad740687cc3ae984e7a9890f178c5c683", null ],
    [ "m_GSDAverageGPUTime", "struct_application_1_1_benchmark_data.html#a6e6cd09ab10a2c88e9aceac2e0c2d1f4", null ],
    [ "m_GSIAverageCPUTime", "struct_application_1_1_benchmark_data.html#a8deeb8a2581f2c38a6bdbc551128f973", null ],
    [ "m_GSIAverageGPUTime", "struct_application_1_1_benchmark_data.html#a235fc8bfeb8ba4d425a081d20b77ab65", null ],
    [ "m_objectCount", "struct_application_1_1_benchmark_data.html#a223989983445ec424ed2c7542bc6f01e", null ],
    [ "m_OS", "struct_application_1_1_benchmark_data.html#a3f868af78c772fc7d733fdfae4671312", null ],
    [ "m_shaderName", "struct_application_1_1_benchmark_data.html#ac4178121907e4a937c5eb3abd9209218", null ],
    [ "m_UUID", "struct_application_1_1_benchmark_data.html#a3b0ad2e5358b7e2c645c6dbc7ccdfe3e", null ],
    [ "m_VSIAverageCPUTime", "struct_application_1_1_benchmark_data.html#a66fed3b1cc2eef342cbc9f3675be9e6c", null ],
    [ "m_VSIAverageGPUTime", "struct_application_1_1_benchmark_data.html#af4fbd1a2fe6d6f2c442856e2282ba96a", null ]
];