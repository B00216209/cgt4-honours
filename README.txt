Linux Dependencies:
libassimp3
libassimp-dev
libglew1.10
libglew-dev
libsdl2-2.0-0
libsdl2-dev
libpoco-dev

Windows Dependencies:
All are contained within the Resources folder, cmake should find them automatically.

Status: 
Win64 AMD R9 290 - Compiles and runs successfully. No warnings or errors.
Ubuntu64 AMD R9 290 - In progress.

Command Line Arguments:
--benchmark : Enables benchmarking mode, this runs each of the 60 shader+renderer+scene combinations for 2000 frames then submits all benchmark data to a server before closing.
	      After the executable closes you will have a link in your clipboard, paste this into a browser to see your results.

Controls:
1 - Use low complexity shader
2 - Use medium complexity shader
3 - Use high complexity shader
w - Previous renderer
e - Next renderer
s - Previous scene
d - Next scene

Notes:
Please give the executable internet/firewall access so it can submit it's benchmark data before closing.
When changing to a new renderer the shader will default to high complexity.
In your IDE of choice set the working directory to be the folder which contains models/shaders/textures and the dynamic link libraries.